<?php

session_start();
include "controller/PropertyController.php";
include "controller/RoomController.php";
include "controller/PhotoController.php";
$path = explode("/", $_SERVER['PHP_SELF']);
$page = substr(array_pop($path), 0, -4);
?>
<header class="header">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="header_content d-flex flex-row align-items-center justify-content-start">
					<div class="logo">
						<a href="index.php">
							<h3 style="color:white;font-weight:800;">Capital</h3>
						</a>
					</div>

					<nav class="main_nav navbar-nav">
						<ul>
							<li class="<?php if ($page == 'index') {echo 'active';} ?>"><a href="index.php">Home</a></li>
							<li class="<?php if ($page == 'about') {echo 'active';} ?>"><a href="about.php">About us</a></li>
							<li class="<?php if ($page == 'properties') {echo 'active';} ?>"><a href="properties.php">Properties</a></li>
							<li class="<?php if ($page == 'news') {echo 'active';} ?>"><a href="news.php">News</a></li>
							<li class="<?php if ($page == 'contact') {echo 'active';} ?>"><a href="contact.php">Contact</a></li>

							<?php if (isset($_SESSION['loginid'])) : ?>
                                <li class="nav-item dropdown <?php if ($page == 'updateProfile') {echo 'active';} ?>">
                                    <a class="nav-link dropdown-toggle" href="updateProfile.php" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <li><a class="dropdown-item <?php if ($page == 'updateProfile') {echo 'active';} ?>" href="updateProfile.php">Edit Profile</a></li>

                                        <li><a class="dropdown-item <?php if ($page == 'postProperty') {echo 'active';} ?>" href="client_addNewProperty.php">Post Property</a></li>

                                        <li><a class="dropdown-item <?php if ($page == 'logout') {echo 'active';} ?>" href="logout.php">Logout</a></li>
                                    </ul>
                                </li>
                                <li><a class="dropdown-item <?php if ($page == 'updateProfile') {echo 'active';} ?>" href="updateProfile.php">Profile</a></li>
                                <li><a class="dropdown-item <?php if ($page == 'postProperty') {echo 'active';} ?>" href="client_addNewProperty.php">Post Property</a></li>
                                <li><a class="dropdown-item <?php if ($page == 'logout') {echo 'active';} ?>" href="logout.php">Logout</a></li>

							<?php else : ?>
								<li class="<?php if ($page == 'login') {echo 'active';} ?>"><a href="login.php">Login</a></li>
							<?php endif; ?>
						</ul>
					</nav>
					<div class="phone_num ml-auto">
						<div class="phone_num_inner">
							<img src="assets/images/phone.png" alt="phone"><span>438-728-1216</span>
						</div>
					</div>
					<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
				</div>
			</div>
		</div>
	</div>
</header>

<!-- Menu -->

<div class="menu trans_500">
	<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
		<div class="menu_close_container">
			<div class="menu_close"></div>
		</div>
		<div class="logo menu_logo">
			<a href="#">
				<div class="logo_container d-flex flex-row align-items-start justify-content-start">
					<div class="logo_image">
						<div><img src="assets/images/logo.png" alt=""></div>
					</div>
				</div>
			</a>
		</div>
		<ul>
			<li class="menu_item"><a href="index.php">Home</a></li>
			<li class="menu_item"><a href="about.php">About us</a></li>
			<li class="menu_item"><a href="properties.php">Properties</a></li>
			<li class="menu_item"><a href="news.php">News</a></li>
			<li class="menu_item"><a href="contact.php">Contact</a></li>
			<li class="menu_item"><a href="updateProfile.php">Edit Profile</a></li>
		</ul>
	</div>
	<div class="menu_phone"><span>call us: </span>438 728 1216</div>
</div>