<?php
    include "head.php";
    include "Container.php";
    include "header.php";
    //$coreController = "Controller/UserController.php";
    //$user_db = new UserDBManager();
    $status = (isset($_GET['success'])) ? $_GET['success'] : null ;  
    $error = (isset($_GET['error'])) ? $_GET['error'] : null ; 
    $property = (isset($_GET['id'])) ? $_GET['id'] : null;
?>
<?php //unset($_SESSION['msg']); ?>

<div class="super_container">

<div class="home">
        <div class="parallax_background parallax-window" data-parallax="scroll" style="background-image:url(assets/images/contact.jpg)" data-speed="0.8"></div>
        <div class="home_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content d-flex flex-row align-items-end justify-content-start">
                            <div class="home_title">Add Room</div>
                            <div class="breadcrumbs ml-auto">
                                <ul>
                                    <li><a href="index.htmo">Home</a></li>
                                    <li>contact</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Search -->
    <div class="home_search">
        <div class="container">
            <div class="row">
            <?php if(isset($_SESSION['msg'])): ?>
                <div class="alert alert-<?php echo $_SESSION['msgType'] ?>">
                    <strong><?php echo $_SESSION['msgType'] ?>!</strong> <?= $_SESSION['msg']; ?>
                </div>
            <?php endif; ?>
        
                <div class="col">
                    <div class="home_search_container">
                        <div class="home_search_content">
                            <form action="" method="post" class="search_form d-flex flex-row align-items-start justfy-content-start">
                                <div class="search_form_content d-flex flex-row align-items-start justfy-content-start flex-wrap">
                                    <div>
                                        <select class="search_form_select">
                                            <option disabled selected>For rent</option>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>
                                    </div>
                                    <div>
                                        <select class="search_form_select">
                                            <option disabled selected>All types</option>
                                            <option>Type 1</option>
                                            <option>Type 2</option>
                                            <option>Type 3</option>
                                            <option>Type 4</option>
                                        </select>
                                    </div>
                                    <div>
                                        <select class="search_form_select">
                                            <option disabled selected>City</option>
                                            <option>New York</option>
                                            <option>Paris</option>
                                            <option>Amsterdam</option>
                                            <option>Rome</option>
                                        </select>
                                    </div>
                                    <div>
                                        <select class="search_form_select">
                                            <option disabled selected>Bedrooms</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                    <div>
                                        <select class="search_form_select">
                                            <option disabled selected>Bathrooms</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                </div>
                                <button class="search_form_button ml-auto">search</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="contact">
        <div class="container">
            <div class="row">
                
                <!-- Contact Form -->
                <div class="col-lg-8">
                    <div class="contact_form_container">
                        <form action="" class="contact_form" enctype="multipart/form-data" id="contact_form" method="post">
                        <input type="hidden" name="listing_id" value="<?= rand(); ?>">
                        <input type="hidden" name="sequence_id" value="<?= rand(); ?>">
                                <!-- Name -->
                                <div >
                                <label for="text-input" class="form-control-label">Description : </label>
                                    <textarea class="contact_textarea contact_input" placeholder="Description..." required="required" name="description"></textarea>
                                </div>
                                <!-- Email -->
                                
                            

                            <div class="row"> 
                                <!-- Name -->
                                <div class="col-lg-6 ">
                                    <label for="text-input" class="form-control-label">Photos : </label>
                                    <input type="file" id="file-input" name="photos[]" class="form-control-file" multiple>
                                </div>
                                <!-- Email -->
                                <div class="col-lg-6">
                                    <label for="text-input" class="form-control-label">Last Updated : </label>
                                    <input type="date" class="contact_input"  name="last_updated" placeholder="Last Updated" required="required">
                                </div>
                            </div>

                            <div class="row"> 
                                <!-- Name -->
                                <div class="col-lg-6 ">
                                    <label for="text-input" class="form-control-label">Photos Last Updated  : </label>
                                    <input type="text" class="contact_input"  name="photos_last_updated" placeholder="Photos Last Updated" required="required">
                                </div>
                                <!-- Email -->
                                <div class="col-lg-6">
                                    <label for="text-input" class="form-control-label">Custom Photo : </label>
                                    <select name="custom_photo" id="select" class="contact_input">
                                                 <option value="0">Yes Or No</option>
                                                 <option value="1">Yes</option>
                                                 <option value="2">No</option>
                                             </select>
                                </div>
                            </div>

                            <button class="contact_button button" name="postPhoto" type="submit">send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="newsletter">
        <div class="parallax_background parallax-window" style="background-image:url(assets/images/newsletter.jpg)" data-speed="0.8"></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="newsletter_content d-flex flex-lg-row flex-column align-items-start justify-content-start">
                        <div class="newsletter_title_container">
                            <div class="newsletter_title">Are you buying or selling?</div>
                            <div class="newsletter_subtitle">Search your dream home</div>
                        </div>
                        <div class="newsletter_form_container">
                            <form action="#" class="newsletter_form">
                                <input type="email" class="newsletter_input" placeholder="Your e-mail address" required="required">
                                <button class="newsletter_button">subscribe now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="assets/styles/bootstrap4/popper.js"></script>
<script src="assets/styles/bootstrap4/bootstrap.min.js"></script>
<script src="assets/plugins/easing/easing.js"></script>
<script src="assets/plugins/rangeslider.js-2.3.0/rangeslider.min.js"></script>
<script src="assets/plugins/parallax-js-master/parallax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
<script src="assets/js/contact.js"></script>

<?php
 include "footer.php";
 
 if (isset($_POST['postPhoto'])) {
    $pc = new PhotoController();
    $pc->postPhoto($_POST, $property);
     }
 ?>