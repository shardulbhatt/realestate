 <?php
    include "admin-header.php";
    $status = (isset($_GET['success'])) ? $_GET['success'] : null;
    $error = (isset($_GET['error'])) ? $_GET['error'] : null;
    $property = (isset($_GET['id'])) ? $_GET['id'] : null;
    /*$room = (isset($_GET['room'])) ? $_GET['room'] : null;*/
    ?>
 <div class="page-container">


     <div class="main-content">
         <div class="section__content section__content--p30">
             <div class="container-fluid">
                 <div class="row">
                     <div class="col-lg-6">
                         <div class="card">
                             <div class="card-header">
                                 <input type="hidden" name="action" value="add">
                                 <strong>Add New Photo</strong>
                             </div>
                             <div class="card-body card-block">
                                 <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                     <input type="hidden" name="listing_id" value="<?= rand(); ?>">
                                     

                                            <input type="hidden" name="sequence_id" value="<?= rand(); ?>">

                                            <div class="row form-group">
                                         <div class="col col-md-3">
                                             <label for="textarea-input" class=" form-control-label">Description</label>
                                         </div>
                                         <div class="col-12 col-md-9">
                                             <textarea name="description" id="textarea-input" rows="5" placeholder="Description..." class="form-control"></textarea>
                                         </div>
                                     </div>

                                     <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="file-input" class=" form-control-label">Photos</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="file" id="file-input" name="photos[]" class="form-control-file" multiple>
                                                </div>
                                            </div>

                                     <div class="row form-group">
                                         <div class="col col-md-3">
                                             <label for="text-input" class=" form-control-label">Last Updated : </label>
                                         </div>
                                         <div class="col-12 col-md-9">
                                             <input type="date" id="text-input" name="last_updated" placeholder="Last Updated" class="form-control">
                                             <!-- <small class="form-text text-muted">This is a help text</small> -->
                                         </div>
                                     </div>

                                     <div class="row form-group">
                                         <div class="col col-md-3">
                                             <label for="text-input" class=" form-control-label">Photos Last Updated  : </label>
                                         </div>
                                         <div class="col-12 col-md-9">
                                             <input type="text" id="text-input" name="photos_last_updated" placeholder="Photos Last Updated" class="form-control">
                                             <!-- <small class="form-text text-muted">This is a help text</small> -->
                                         </div>
                                     </div>

                                     <div class="row form-group">
                                         <div class="col col-md-3">
                                             <label for="select" class=" form-control-label">Custom Photo</label>
                                         </div>
                                         <div class="col-12 col-md-9">
                                             <select name="custom_photo" id="select" class="form-control">
                                                 <option value="0">Yes Or No</option>
                                                 <option value="1">Yes</option>
                                                 <option value="2">No</option>
                                             </select>
                                         </div>
                                     </div>



                                     <div class="row form-group">
                                         <div class="col col-md-3">
                                             <label for="select" class=" form-control-label">Status</label>
                                         </div>
                                         <div class="col-12 col-md-9">
                                             <select name="status" id="select" class="form-control">
                                                 <option value="0">Please select Status</option>
                                                 <option value="1">0</option>
                                                 <option value="2">1</option>
                                             </select>
                                         </div>
                                     </div>
                                   
                             </div>
                             <div class="card-footer">
                                 <button type="submit" class="btn btn-primary btn-sm" name="addPhoto">
                                     <i class="fa fa-dot-circle-o"></i> Add Photo
                                 </button>

                                 <button type="reset" class="btn btn-danger btn-sm">
                                     <i class="fa fa-ban"></i> Reset
                                 </button>

                                 <?php if (!$error && $status && $status == "true") : ?>
                                     <div class="alert alert-success mt-2" role="alert">
                                         The photo has been insert successfully.
                                     </div>
                                 <?php elseif ($error && $error == 4001) : ?>
                                     <div class="alert alert-danger mt-2" role="alert">
                                         Something went wrong.
                                     </div>
                                 <?php endif; ?>
                             </div>
                         </div>

                     </div>


                 </div>
                 <?php
                    include "admin-footer.php";
                    if (isset($_POST['addPhoto'])) {
                        $pc = new PhotoController();
                        $pc->addPhoto($_POST, $property);
                    }
                ?>
             </div>
         </div>
     </div>
 </div>

 </div>

 </div>


 <script src="../realestate/assets/vendor/jquery-3.2.1.min.js"></script>
 <!-- Bootstrap JS-->
 <script src="../realestate/assets/vendor/bootstrap-4.1/popper.min.js"></script>
 <script src="../realestate/assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
 <!-- Vendor JS       -->
 <script src="../realestate/assets/vendor/slick/slick.min.js">
 </script>
 <script src="../realestate/assets/vendor/wow/wow.min.js"></script>
 <script src="../realestate/assets/vendor/animsition/animsition.min.js"></script>
 <script src="../realestate/assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
 </script>
 <script src="../realestate/assets/vendor/counter-up/jquery.waypoints.min.js"></script>
 <script src="../realestate/assets/vendor/counter-up/jquery.counterup.min.js">
 </script>
 <script src="../realestate/assets/vendor/circle-progress/circle-progress.min.js"></script>
 <script src="../realestate/assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
 <script src="../realestate/assets/vendor/chartjs/Chart.bundle.min.js"></script>
 <script src="../realestate/assets/vendor/select2/select2.min.js">
 </script>

 <!-- Main JS-->
 <script src="../assets/js/main.js"></script>

 <script src="../assets/vendor/jquery-3.2.1.min.js"></script>
 <!-- Bootstrap JS-->
 <script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
 <script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
 <!-- Vendor JS       -->
 <script src="../assets/vendor/slick/slick.min.js">
 </script>
 <script src="../assets/vendor/wow/wow.min.js"></script>
 <script src="../assets/vendor/animsition/animsition.min.js"></script>
 <script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
 </script>
 <script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
 <script src="../assets/vendor/counter-up/jquery.counterup.min.js">
 </script>
 <script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
 <script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
 <script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
 <script src="../assets/vendor/select2/select2.min.js">
 </script>
 <!-- Main JS-->
 <script src="../assets/js/main.js"></script>