<?php
include "admin-header.php";

if(!isset($_GET['id']))
{
    echo "<script>window.history.back();</script>";
}
$id = $_GET['id'];
$rc = new PropertyRoomDBManager();
$res = $rc->deleteRoom($id);
echo "<script>window.history.back();</script>";
exit;