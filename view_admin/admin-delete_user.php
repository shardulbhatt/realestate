<?php
include "admin-header.php";

if(!isset($_GET['id']))
{
    echo "<script>window.history.back();</script>";
}
$id = $_GET['id'];
$uc = new UserDBManager();
$res = $uc->deleteUser($id);
echo "<script>window.history.back();</script>";
exit;