 <?php
    include "admin-header.php";

 $rc = new RoomController();
$roomdata = $rc->getSingleRoom($_GET['id']);

$status = (isset($_GET['success'])) ? $_GET['success'] : null;
$error = (isset($_GET['error'])) ? $_GET['error'] : null;

     ?>
 <div class="page-container">


     <div class="main-content">
         <div class="section__content section__content--p30">
             <div class="container-fluid">
                 <div class="row">
                     <div class="col-lg-6">
                         <div class="card">
                             <div class="card-header">
                                 <input type="hidden" name="action" value="add">
                                 <strong>Update Room</strong>
                             </div>
                             <div class="card-body card-block">
                                 <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    
                                     <input type="hidden" name="room_id" value="<?= $roomdata->getRoom_id() ;?>">

                                     <input type="hidden" name="listing_id" value="<?= $roomdata->getListing_id() ;?>">
                                     <input type="hidden" name="tbl_property_id" value="<?= $roomdata->getTbl_property_id() ;?>">
                                    
                                     <div class="row form-group">
                                         <div class="col col-md-3">
                                             <label for="text-input" class=" form-control-label">Room Type : </label>
                                         </div>
                                         <div class="col-12 col-md-9">
                                             <input type="text" id="text-input" name="type" placeholder="Room Type" class="form-control" value="<?= $roomdata->getType() ;?>">
                                            
                                         </div>
                                     </div>

                                     <div class="row form-group">
                                         <div class="col col-md-3">
                                             <label for="text-input" class=" form-control-label">Room Width : </label>
                                         </div>
                                         <div class="col-12 col-md-9">
                                             <input type="text" id="text-input" name="width" placeholder="Room Width" class="form-control" value="<?= $roomdata->getWidth() ;?>">
                                                                                   </div>
                                     </div>


                                     <div class="row form-group">
                                         <div class="col col-md-3">
                                             <label for="text-input" class=" form-control-label">Room Length : </label>
                                         </div>
                                         <div class="col-12 col-md-9">
                                             <input type="text" id="text-input" name="length" placeholder="Room Length" class="form-control" value="<?= $roomdata->getLength() ;?>">
                                             <!-- <small class="form-text text-muted">This is a help text</small> -->
                                         </div>
                                     </div>

                                     <div class="row form-group">
                                         <div class="col col-md-3">
                                             <label for="text-input" class=" form-control-label">Room Level : </label>
                                         </div>
                                         <div class="col-12 col-md-9">
                                             <input type="text" id="text-input" name="level" placeholder="Room Length" class="form-control" value="<?= $roomdata->getLevel() ;?>">
                                             <!-- <small class="form-text text-muted">This is a help text</small> -->
                                         </div>
                                     </div>




                                     <div class="row form-group">
                                         <div class="col col-md-3">
                                             <label for="textarea-input" class=" form-control-label">Room Dimensions</label>
                                         </div>
                                         <div class="col-12 col-md-9">
                                             <textarea name="dimension" id="textarea-input" rows="5" placeholder="Room Dimensions..." class="form-control" ><?= $roomdata->getDimension() ?></textarea>
                                         </div>
                                     </div>

                                     <div class="row form-group">
                                         <div class="col col-md-3">
                                             <label for="select" class=" form-control-label">Custom Room</label>
                                         </div>
                                         <div class="col-12 col-md-9">
                                             <select name="custom_room" id="select" class="form-control">
                                                 <option value="0">Yes Or No</option>
                                                 <option value="1" <?= ($roomdata->getStatus() == '1') ? 'selected="selected"' : ''; ?>>Yes</option>
                                                 <option value="2" <?= ($roomdata->getStatus() == '2') ? 'selected="selected"' : ''; ?>>No</option>
                                             </select>
                                         </div>
                                     </div>



                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="select" class=" form-control-label">Status</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <select name="status" id="select" class="form-control" >
                                                <option value="5">Please select Status</option>
                                               <option value="1" <?= ($roomdata->getStatus() == '1') ? 'selected="selected"' : ''; ?>>0</option>
                                                     <option value="2" <?= ($roomdata->getStatus() == '2') ? 'selected="selected"' : ''; ?>>1</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                             </div>
                             <div class="card-footer">
                                 <button type="submit" class="btn btn-primary btn-sm" name="update">
                                     <i class="fa fa-dot-circle-o"></i> Update Room
                                 </button>

                                 <button type="reset" class="btn btn-danger btn-sm">
                                     <i class="fa fa-ban"></i> Reset
                                 </button>

                                 <?php if (!$error && $status && $status == "true") : ?>
                                     <div class="alert alert-success mt-2" role="alert">
                                         The room has been insert successfully.
                                     </div>
                                 <?php elseif ($error && $error == 4001) : ?>
                                     <div class="alert alert-danger mt-2" role="alert">
                                         Something went wrong.
                                     </div>
                                 <?php endif; ?>
                             </div>
                         </div>

                     </div>


                 </div>
                 <?php
                    include "admin-footer.php";
                    if (isset($_POST['update'])) {
        $rc->updateRoom($_POST);
    }
                ?>
             </div>
         </div>
     </div>
 </div>

 </div>

 </div>


 <script src="../realestate/assets/vendor/jquery-3.2.1.min.js"></script>
 <!-- Bootstrap JS-->
 <script src="../realestate/assets/vendor/bootstrap-4.1/popper.min.js"></script>
 <script src="../realestate/assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
 <!-- Vendor JS       -->
 <script src="../realestate/assets/vendor/slick/slick.min.js">
 </script>
 <script src="../realestate/assets/vendor/wow/wow.min.js"></script>
 <script src="../realestate/assets/vendor/animsition/animsition.min.js"></script>
 <script src="../realestate/assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
 </script>
 <script src="../realestate/assets/vendor/counter-up/jquery.waypoints.min.js"></script>
 <script src="../realestate/assets/vendor/counter-up/jquery.counterup.min.js">
 </script>
 <script src="../realestate/assets/vendor/circle-progress/circle-progress.min.js"></script>
 <script src="../realestate/assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
 <script src="../realestate/assets/vendor/chartjs/Chart.bundle.min.js"></script>
 <script src="../realestate/assets/vendor/select2/select2.min.js">
 </script>

 <!-- Main JS-->
 <script src="../assets/js/main.js"></script>

 <script src="../assets/vendor/jquery-3.2.1.min.js"></script>
 <!-- Bootstrap JS-->
 <script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
 <script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
 <!-- Vendor JS       -->
 <script src="../assets/vendor/slick/slick.min.js">
 </script>
 <script src="../assets/vendor/wow/wow.min.js"></script>
 <script src="../assets/vendor/animsition/animsition.min.js"></script>
 <script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
 </script>
 <script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
 <script src="../assets/vendor/counter-up/jquery.counterup.min.js">
 </script>
 <script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
 <script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
 <script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
 <script src="../assets/vendor/select2/select2.min.js">
 </script>
 <!-- Main JS-->
 <script src="../assets/js/main.js"></script>