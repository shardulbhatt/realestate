 <?php
    include "admin-header.php";
    include "../head.php";
    $coreController = "../controller/PhotoController.php";
    $photo_db = new PropertyPhotosDBManager();
?>
<div class="page-container">

                
                  <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                                                   </div>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Photos data table</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-right">
                                            <a href="admin-addNewUser.php" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="collapse" >
                                            
                                                        <b>Add User</b></a>&nbsp;
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                <?php if(isset($_SESSION['msg'])): ?>
                                        <div class="alert alert-<?php echo $_SESSION['msgType'] ?>">
                    <strong><?php echo $_SESSION['msgType'] ?>!</strong> <?= $_SESSION['msg']; ?>
                </div>
            <?php endif; ?>
                 <div class="row m-t-30">
                            <div class="col-md-12">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                       

                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                               
                                                <th>Photo Id(details Id)</th>
                                                <th>Listing Id</th>
                                                <th>Sequence Id</th>
                                                <th>Description</th>
                                                <th>Photos</th>
                                                <th>Last Updated</th>
                                                <th>Photo Last Updated</th>
                                                <th>Custom Photo</th>
                                                <th>Room ID</th>
                                                <th>Property ID</th>
                                                <th>Status</th>
                                                <th>CRUD</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($photo_db->getAllPhotos() as $photo) :?>
                                            <tr class="tr-shadow">
                                                <td><?= $photo->getDetails_id() ?></td>
                                                <td><?= $photo->getListing_id() ?></td>
                                                 <td> <?= $photo->getSequence_id() ?></td>   
                                                <td><?= $photo->getDescription() ?></td>
                                                <td ><?= $photo->getPhotos() ?></td>
                                                <td><?= $photo->getLast_updated() ?></td>
                                                <td><?= $photo->getPhotos_last_updated() ?></td>
                                                 <td><?= $photo->getCustom_photo() ?></td>   
                                                <td><?= $photo->getTbl_rooms_id() ?></td>
                                                <td><?= $photo->getTbl_property_id() ?></td>
                                                <td><?= $photo->getStatus() ?></td>
                                            
                                                <td>
                                                    <div class="table-data-feature">
                                                        <a href="admin-updatePhoto.php?id=<?= $photo->getDetails_id() ?>" class="btn btn-sm btn-warning" data-toggle="collapse" data-target="#editPhoto">
                                                        <b>Edit</b></a>&nbsp;

                                                        <a href="<?php echo $coreController ?>?action=deleteUser&id=<?= $photo->getDetails_id() ?>" onclick="return confirm('Are you sure you want to delete <?= $photo->getDescription()?>')"class="btn btn-sm btn-danger"><b>Delete</b></a>&nbsp;
                                                        
                                                        <a href="<?php echo $coreController ?>?action=ban&id=<?= $photo->getDetails_id() ?>" onclick="return confirm('Are you sure you want to ban <?= $photo->getDescription()?>')"class="btn btn-sm btn-dark"><b>Ban</b></a>&nbsp;

                                                      
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="spacer"></tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        <div class="row m-t-30">
                            <div class="col-md-12">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                                    <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                               <th>Photo Id(details Id)</th>
                                                <th>Listing Id</th>
                                                <th>Sequence Id</th>
                                                <th>Description</th>
                                                <th>Photos</th>
                                                <th>Last Updated</th>
                                                <th>Photo Last Updated</th>
                                                <th>Custom Photo</th>
                                                <th>Room ID</th>
                                                <th>Property ID</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($photo_db->getAllPhotos() as $photo) :?>
                                            <tr>
                                               <td><?= $photo->getDetails_id() ?></td>
                                                <td><?= $photo->getListing_id() ?></td>
                                                 <td> <?= $photo->getSequence_id() ?></td>   
                                                <td><?= $photo->getDescription() ?></td>
                                                <td ><?= $photo->getPhotos() ?></td>
                                                <td><?= $photo->getLast_updated() ?></td>
                                                <td><?= $photo->getPhotos_last_updated() ?></td>
                                                 <td><?= $photo->getCustom_photo() ?></td>   
                                                <td><?= $photo->getTbl_rooms_id() ?></td>
                                                <td><?= $photo->getTbl_property_id() ?></td>
                                                <td><?= $photo->getStatus() ?></td>
                                            

                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE-->
                            </div>
                        </div>

                   <?php 
                      include "admin-footer.php";
                 ?>  
                    </div>
                </div>
            </div>
        </div>
</div>
            
                    
                    

           

    <script src="../assets/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../assets/vendor/slick/slick.min.js">
    </script>
    <script src="../assets/vendor/wow/wow.min.js"></script>
    <script src="../assets/vendor/animsition/animsition.min.js"></script>
    <script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../assets/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../assets/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../assets/js/main.js"></script>

    <script src="../assets/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../assets/vendor/slick/slick.min.js">
    </script>
    <script src="../assets/vendor/wow/wow.min.js"></script>
    <script src="../assets/vendor/animsition/animsition.min.js"></script>
    <script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../assets/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../assets/vendor/select2/select2.min.js">
    </script>
    <!-- Main JS-->
    <script src="../assets/js/main.js"></script>