<?php
include "admin-header.php";

if(!isset($_GET['id']))
{
    echo "<script>window.history.back();</script>";
}
$id = $_GET['id'];
$pc = new PropertyDBManager();
$res = $pc->deleteProperty($id);
echo "<script>window.history.back();</script>";
exit;