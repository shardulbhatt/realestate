 <?php
    include "admin-header.php";
    include "../head.php";
    $coreController = "Controller/PropertyController.php";
    $property_db = new PropertyDBManager();
    ?>
 <div class="page-container">


     <div class="main-content">
         <div class="section__content section__content--p30">
             <div class="container-fluid">
                 <div class="row">
                 </div>

                 <div class="row">
                     <div class="col-md-12">
                         <!-- DATA TABLE -->
                         <h3 class="title-5 m-b-35">Property data table</h3>
                         <div class="table-data__tool">
                             <div class="table-data__tool-right">
                                 <a href="admin-addNewProperty.php" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="collapse">

                                     <b>Add Property</b></a>&nbsp;
                             </div>
                         </div>
                         <div class="table-responsive table-responsive-data2">
                             <?php if (isset($_SESSION['msg'])) : ?>
                                 <div class="alert alert-<?php echo $_SESSION['msgType'] ?>">
                                     <strong><?php echo $_SESSION['msgType'] ?>!</strong> <?= $_SESSION['msg']; ?>
                                 </div>
                             <?php endif; ?>

                         </div>
                     </div>
                 </div>
                 <div class="row m-t-30">
                     <div class="col-md-12">
                         <!-- DATA TABLE-->
                         <div class="table-responsive m-b-40">
                             <table class="table table-borderless table-data3">
                                 <thead>
                                     <tr>

                                         <th>Property ID</th>
                                         <th>User ID</th>
                                         <th>Last Updated</th>
                                         <th>Price</th>
                                         <th>Bathroom(s)</th>
                                         <th>Bedroom(s)</th>
                                         
                                         <th>Action</th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     <?php foreach ($property_db->getAllProperties() as $properties) : ?>
                                         <tr>

                                             <td><?= $properties->getProperty_id() ?></td>
                                             <td><?= $properties->getTbl_users_id() ?></td>
                                             <td><?= $properties->getLast_updated() ?></td>
                                            
                                             <td><?= $properties->getPrice() ?></td>
                                             <td><?= $properties->getBathroom_total() ?></td>

                                             <td><?= $properties->getBedroom_total() ?></td>
                                             
                                             <td>
                                                 <div class="table-data-feature">
                                                     <a href="admin-updateProperty.php?id=<?= $properties->getProperty_id() ?>" class="btn btn-sm btn-warning" data-toggle="collapse" data-target="#editProperty">
                                                         <b>Edit</b></a>&nbsp;
                                                     <button onclick="del_Property(<?= $properties->getProperty_id() ?> , '<?= $properties->getStreet_address() ?>');" class="btn btn-sm btn-danger"><b>Delete</b></button>
                                                     <a href="<?php echo $coreController ?>?action=ban&id=<?= $properties->getProperty_id() ?>" onclick="return confirm('Are you sure you want to ban <?= $properties->getFeatures() ?>')" class="btn btn-sm btn-dark ml-1"><b>Ban</b></a>&nbsp;
                                                 </div>
                                             </td>


                                         </tr>

                                     <?php endforeach; ?>
                                 </tbody>
                             </table>
                         </div>
                         <!-- END DATA TABLE-->
                     </div>
                 </div>
                 <?php
                    include "admin-footer.php";
                    ?>
             </div>
         </div>
     </div>
 </div>
 </div>






 <script src="../assets/vendor/jquery-3.2.1.min.js"></script>
 <!-- Bootstrap JS-->
 <script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
 <script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
 <!-- Vendor JS       -->
 <script src="../assets/vendor/slick/slick.min.js">
 </script>
 <script src="../assets/vendor/wow/wow.min.js"></script>
 <script src="../assets/vendor/animsition/animsition.min.js"></script>
 <script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
 </script>
 <script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
 <script src="../assets/vendor/counter-up/jquery.counterup.min.js">
 </script>
 <script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
 <script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
 <script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
 <script src="../assets/vendor/select2/select2.min.js">
 </script>

 <!-- Main JS-->
 <script src="../assets/js/main.js"></script>

 <script src="../assets/vendor/jquery-3.2.1.min.js"></script>
 <!-- Bootstrap JS-->
 <script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
 <script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
 <!-- Vendor JS       -->
 <script src="../assets/vendor/slick/slick.min.js">
 </script>
 <script src="../assets/vendor/wow/wow.min.js"></script>
 <script src="../assets/vendor/animsition/animsition.min.js"></script>
 <script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
 </script>
 <script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
 <script src="../assets/vendor/counter-up/jquery.counterup.min.js">
 </script>
 <script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
 <script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
 <script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
 <script src="../assets/vendor/select2/select2.min.js">
 </script>
 <!-- Main JS-->
 <script src="../assets/js/main.js"></script>
 <script>
     function del_Property(id, address) {
         if (confirm("Are you sure want to delete this" + address + "?")) {
             document.location.href = 'admin-delete_property.php?id=' + id;
         }
     }
 </script>