 <?php
    include "admin-header.php";
    $uc = new UserController();
    $user = $uc->getSingleUser($_GET['id']);

    $status = (isset($_GET['success'])) ? $_GET['success'] : null;
    $error = (isset($_GET['error'])) ? $_GET['error'] : null;

    ?>
 <div class="page-container">


     <div class="main-content">
         <div class="section__content section__content--p30">
             <div class="container-fluid">
                 <div class="row">
                     <div class="col-lg-6">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Update User</strong>
                             </div>
                             <div class="card-body card-block">
                                 <tr id="editUse" class="collapse fade">
                                     <form action="" method="post" class="form-horizontal">
                                         <input type="hidden" name="id" value="<?= $user->getId() ?>">
                                         <div class="row form-group">
                                             <div class="col col-md-3">
                                                 <label class=" form-control-label">Username</label>
                                             </div>
                                             <div class="col-12 col-md-9">
                                                 <input class="form-control" name="username" value="<?= $user->getUsername(); ?>" readonly>
                                             </div>
                                         </div>
                                         <div class="row form-group">
                                             <div class="col col-md-3">
                                                 <label for="text-input" class=" form-control-label">Password</label>
                                             </div>
                                             <div class="col-12 col-md-9">
                                                 <input type="text" id="text-input" name="password" placeholder="Password" class="form-control" value="<?= $user->getPassword() ?>"> </div>
                                         </div>
                                         <div class="row form-group">
                                             <div class="col col-md-3">
                                                 <label for="text-input" class=" form-control-label">Full Name</label>
                                             </div>
                                             <div class="col-12 col-md-9">
                                                 <input type="text" id="text-input" name="fullname" placeholder="Full Name" class="form-control" value="<?= $user->getFullname(); ?>">
                                             </div>
                                         </div>

                                         <div class="row form-group">
                                             <div class="col col-md-3">
                                                 <label for="text-input" class=" form-control-label">Date Of Birth</label>
                                             </div>
                                             <div class="col-12 col-md-9">
                                                 <input type="date" id="text-input" name="dob" placeholder="Date Of Birth" class="form-control" value="<?= $user->getDob(); ?>">
                                             </div>
                                         </div>

                                         <div class="row form-group">
                                             <div class="col col-md-3">
                                                 <label for="email-input" class=" form-control-label">Email </label>
                                             </div>
                                             <div class="col-12 col-md-9">
                                                 <input type="email" id="email-input" name="email" placeholder="Email" class="form-control" value="<?= $user->getEmail(); ?>" readonly>

                                             </div>
                                         </div>
                                         <div class="row form-group">
                                             <div class="col col-md-3">
                                                 <label for="text-input" class=" form-control-label">Phone</label>
                                             </div>
                                             <div class="col-12 col-md-9">
                                                 <input type="text" id="text-input" name="phone" placeholder="Phone" class="form-control" value="<?= $user->getPhone() ?>">

                                             </div>
                                         </div>
                                         <div class="row form-group">
                                             <div class="col col-md-3">
                                                 <label for="textarea-input" class=" form-control-label">Address</label>
                                             </div>
                                             <div class="col-12 col-md-9">
                                                 <textarea name="address" id="textarea-input" rows="5" placeholder="Address..." class="form-control"><?= $user->getAddress() ?></textarea>
                                             </div>
                                         </div>
                                         <div class="row form-group">
                                             <div class="col col-md-3">
                                                 <label for="select" class=" form-control-label">Level</label>
                                             </div>
                                             <div class="col-12 col-md-9">
                                                 <select name="level" id="select" class="form-control">
                                                     <option value="0">Please select Level</option>
                                                     <option value="1" <?= ($user->getLevel() == '1') ? 'selected="selected"' : ''; ?>>1</option>
                                                     <option value="2" <?= ($user->getLevel() == '2') ? 'selected="selected"' : ''; ?>>2</option>
                                                     <option value="3" <?= ($user->getLevel() == '3') ? 'selected="selected"' : ''; ?>>3</option>
                                                     <option value="4" <?= ($user->getLevel() == '4') ? 'selected="selected"' : ''; ?>>4</option>
                                                 </select>
                                             </div>
                                         </div>
                                         <div class="row form-group">
                                             <div class="col col-md-3">
                                                 <label for="select" class=" form-control-label">Status</label>
                                             </div>
                                             <div class="col-12 col-md-9">
                                                 <select name="status" id="select" class="form-control">
                                                     <option value="0">Please select Status</option>
                                                     <option value="1" <?= ($user->getStatus() == '1') ? 'selected="selected"' : ''; ?>>0</option>
                                                     <option value="2" <?= ($user->getStatus() == '2') ? 'selected="selected"' : ''; ?>>1</option>
                                                 </select>
                                             </div>
                                         </div>
                                 </tr>
                             </div>
                             <div class="card-footer">
                                 <button type="submit" name="update" class="btn btn-primary btn-sm">
                                     <i class="fa fa-dot-circle-o"></i> Update User
                                 </button>
                                 <?php if ($error && $error == 1001) : ?>
                                     <div class="alert alert-danger mt-2" role="alert">
                                         Error in update process.
                                     </div>
                                 <?php elseif ($error && $error == 1000) : ?>
                                     <div class="alert alert-danger mt-2" role="alert">
                                         All fields are reuired.
                                     </div>
                                 <?php endif; ?>
                             </div>
                             </form>
                         </div>
                     </div>




                 </div>
                 <?php
                    include "admin-footer.php";
                    /*  if(isset($_POST['editUser']))
    {
        $uc = new UserController();
        $uc->editUser($_POST, true);
    }*/
                    ?>
             </div>
         </div>
     </div>
 </div>

 </div>

 </div>


 <script src="../assets/vendor/jquery-3.2.1.min.js"></script>
 <!-- Bootstrap JS-->
 <script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
 <script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
 <!-- Vendor JS       -->
 <script src="../assets/vendor/slick/slick.min.js">
 </script>
 <script src="../assets/vendor/wow/wow.min.js"></script>
 <script src="../assets/vendor/animsition/animsition.min.js"></script>
 <script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
 </script>
 <script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
 <script src="../assets/vendor/counter-up/jquery.counterup.min.js">
 </script>
 <script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
 <script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
 <script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
 <script src="../assets/vendor/select2/select2.min.js">
 </script>

 <!-- Main JS-->
 <script src="../assets/js/main.js"></script>

 <script src="../assets/vendor/jquery-3.2.1.min.js"></script>
 <!-- Bootstrap JS-->
 <script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
 <script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
 <!-- Vendor JS       -->
 <script src="../assets/vendor/slick/slick.min.js">
 </script>
 <script src="../assets/vendor/wow/wow.min.js"></script>
 <script src="../assets/vendor/animsition/animsition.min.js"></script>
 <script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
 </script>
 <script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
 <script src="../assets/vendor/counter-up/jquery.counterup.min.js">
 </script>
 <script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
 <script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
 <script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
 <script src="../assets/vendor/select2/select2.min.js">
 </script>
 <!-- Main JS-->
 <script src="../assets/js/main.js"></script>



 <script src="../assets/vendor/jquery-3.2.1.min.js"></script>
 <!-- Bootstrap JS-->
 <script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
 <script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
 <!-- Vendor JS       -->
 <script src="../assets/vendor/slick/slick.min.js">
 </script>
 <script src="../assets/vendor/wow/wow.min.js"></script>
 <script src="../assets/vendor/animsition/animsition.min.js"></script>
 <script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
 </script>
 <script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
 <script src="../assets/vendor/counter-up/jquery.counterup.min.js">
 </script>
 <script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
 <script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
 <script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
 <script src="../assets/vendor/select2/select2.min.js">
 </script>

 <!-- Main JS-->
 <script src="../assets/js/main.js"></script>

 <?php
    if (isset($_POST['update'])) {
        $uc->updateUser($_POST);
    }
?>