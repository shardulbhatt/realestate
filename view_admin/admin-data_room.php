 <?php
    include "admin-header.php";
    include "../head.php";
    $coreController = "../controller/RoomController.php";
    $room_db = new PropertyRoomDBManager();
?>
<div class="page-container">

                
                  <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                                                   </div>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Room data table</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-right">
                                            <a href="admin-addNewUser.php" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="collapse" >
                                            
                                                        <b>Add User</b></a>&nbsp;
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                <?php if(isset($_SESSION['msg'])): ?>
                                        <div class="alert alert-<?php echo $_SESSION['msgType'] ?>">
                    <strong><?php echo $_SESSION['msgType'] ?>!</strong> <?= $_SESSION['msg']; ?>
                </div>
            <?php endif; ?>
                 <div class="row m-t-30">
                            <div class="col-md-12">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                       

                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                               
                                                <th>ID</th>
                                                <th>Listing Id</th>
                                                <th>type</th>
                                                <th>width</th>
                                                <th>Length</th>
                                                <th>Level</th>
                                                <th>Dimension</th>
                                                <th>Custom Room</th>
                                                <th>Property ID</th>
                                                <th>Status</th>
                                                <th>CRUD</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($room_db->getAllRooms() as $room) :?>
                                            <tr class="tr-shadow">
                                                <td><?= $room->getRoom_id() ?></td>
                                                <td><?= $room->getListing_id() ?></td>
                                                 <td> <?= $room->getType() ?></td>   
                                                <td><?= $room->getWidth() ?></td>
                                                <td ><?= $room->getLength() ?></td>
                                                <td><?= $room->getLevel() ?></td>
                                                <td><?= $room->getDimension() ?></td>
                                                 <td><?= $room->getCustom_room() ?></td>   
                                                <td><?= $room->getTbl_property_id() ?></td>
                                                <td><?= $room->getStatus() ?></td>
                                            
                                                <td>
                                                    <div class="table-data-feature">
                                                        <a href="admin-updateRoom.php?id=<?= $room->getRoom_id() ?>" class="btn btn-sm btn-warning" data-toggle="collapse" data-target="#editRoom">
                                                        <b>Edit</b></a>&nbsp;

                                                        <button onclick="del_Room(<?= $room->getRoom_id() ?> , '<?= $room->getDimension() ?>');" class="btn btn-sm btn-danger"><b>Delete</b></button>
                                                        
                                                        <a href="<?php echo $coreController ?>?action=ban&id=<?= $room->getRoom_id() ?>" onclick="return confirm('Are you sure you want to ban <?= $room->getType()?>')"class="btn btn-sm btn-dark"><b>Ban</b></a>&nbsp;

                                                        <a href="admin-updateUser.php" class="btn btn-sm btn-primary" data-toggle="collapse" data-target="#editUser<?= $room->getRoom_id() ?>">
                                                        <b>Photos Of Room</b></a>&nbsp;
                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="spacer"></tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        <div class="row m-t-30">
                            <div class="col-md-12">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                                    <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                               <th>ID</th>
                                                <th>Listing Id</th>
                                                <th>type</th>
                                                <th>width</th>
                                                <th>Length</th>
                                                <th>Level</th>
                                                <th>Dimension</th>
                                                <th>Custom Room</th>
                                                <th>Property ID</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($room_db->getAllRooms() as $room) :?>
                                            <tr>
                                                <td><?= $room->getRoom_id() ?></td>
                                                <td><?= $room->getListing_id() ?></td>
                                                 <td> <?= $room->getType() ?></td>   
                                                <td><?= $room->getWidth() ?></td>
                                                <td ><?= $room->getLength() ?></td>
                                                <td><?= $room->getLevel() ?></td>
                                                <td><?= $room->getDimension() ?></td>
                                                 <td><?= $room->getCustom_room() ?></td>   
                                                <td><?= $room->getTbl_property_id() ?></td>
                                                <td><?= $room->getStatus() ?></td>

                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE-->
                            </div>
                        </div>

                   <?php 
                      include "admin-footer.php";
                 ?>  
                    </div>
                </div>
            </div>
        </div>
</div>
            
                    
                    

           

    <script src="../assets/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../assets/vendor/slick/slick.min.js">
    </script>
    <script src="../assets/vendor/wow/wow.min.js"></script>
    <script src="../assets/vendor/animsition/animsition.min.js"></script>
    <script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../assets/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../assets/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="../assets/js/main.js"></script>

    <script src="../assets/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="../assets/vendor/slick/slick.min.js">
    </script>
    <script src="../assets/vendor/wow/wow.min.js"></script>
    <script src="../assets/vendor/animsition/animsition.min.js"></script>
    <script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="../assets/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="../assets/vendor/select2/select2.min.js">
    </script>
    <!-- Main JS-->
    <script src="../assets/js/main.js"></script>
    <script>
    function del_Room(id, dimension){
        if(confirm("Are you sure want to delete the room with dimension of : " + dimension +"?")){
            document.location.href = 'admin-delete_room.php?id='+id;
        }
    }
</script>