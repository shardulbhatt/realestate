<?php
include "admin-header.php";
$pc = new PropertyController();
$propertydata = $pc->getSingleProperty($_GET['id']);
$status = (isset($_GET['success'])) ? $_GET['success'] : null;
$error = (isset($_GET['error'])) ? $_GET['error'] : null;

$coreController = "../controller/RoomController.php";
$room_db = new PropertyRoomDBManager();

/*$rc = new RoomController();
$roomdata = $rc->getSingleRoom($_GET['id']);
$status = (isset($_GET['success'])) ? $_GET['success'] : null;
$error = (isset($_GET['error'])) ? $_GET['error'] : null;
*/
$coreController = "../controller/PhotoController.php";
$photo_db = new PropertyPhotosDBManager();
/*
$photocon = new PhotoController();
$photodata = $photocon->getSinglePhoto($_GET['id']);
$status = (isset($_GET['success'])) ? $_GET['success'] : null;
$error = (isset($_GET['error'])) ? $_GET['error'] : null;*/

?>

<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <input type="hidden" name="action" value="add">
                                <strong>Add New Property</strong>
                            </div>
                            <div class="card-body card-block">
                                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <input type="hidden" name="property_id" value="<?= $propertydata->getProperty_id() ;?>">
                                <input type="hidden" name="tbl_users_id" value="<?= $propertydata->getTbl_users_id() ;?>">

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class="form-control-label">Last Updated</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="date" id="text-input" name="last_updated" placeholder="Last Updated" class="form-control"  value="<?= $propertydata->getLast_updated() ;?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="textarea-input" class="form-control-label">Features</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <textarea name="features" id="textarea-input" rows="5" placeholder="Features..." class="form-control"><?= $propertydata->getFeatures() ?></textarea>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class="form-control-label">Parking Space</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="parking_space_total" placeholder="Parking Space" class="form-control" value="<?= $propertydata->getParking_space_total() ?>">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class="form-control-label">Price</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="number" id="text-input" name="price" placeholder="Price" class="form-control"  value="<?= $propertydata->getPrice() ?>">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Property Type</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="property_type" placeholder="Property Type" class="form-control" value="<?= $propertydata->getProperty_type() ?>">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="textarea-input" class="form-control-label">Public Remarks</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <textarea name="public_remarks" id="textarea-input" rows="5" placeholder="Public Remarks..." class="form-control" ><?= $propertydata->getPublic_remarks() ?></textarea>
                                        </div>
                                    </div>
 
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class="form-control-label">Structure</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="structure" placeholder="Structure" class="form-control" value="<?= $propertydata->getStructure() ?>"  >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class="form-control-label">Total Bathrooms</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="number" id="text-input" name="bathroom_total" placeholder="Number Of Bathrooms" class="form-control" value="<?= $propertydata->getBathroom_total() ?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class="form-control-label">Total Bedrooms</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="number" id="text-input" name="bedroom_total" placeholder="Number Of Bedrooms" class="form-control" value="<?= $propertydata->getBedroom_total() ?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>


                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="email-input" class="form-control-label">Architectural Style </label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="email-input" name="architectural_style" placeholder="Architectural Style" class="form-control" value="<?= $propertydata->getArchitectural_style() ?>" >
                                            <!-- <small class="help-block form-text">Please enter your email</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class="form-control-label">Construction Date</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="date" id="text-input" name="constructed_date" placeholder="Construction Date" class="form-control" value="<?= $propertydata->getConstructed_date() ?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class="form-control-label">Exterior Finish</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="exterior_finish" placeholder="Exterior Finish" class="form-control" value="<?= $propertydata->getExterior_finish() ?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class="form-control-label">Flooring Type</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="flooring_type" placeholder="Flooring Type" class="form-control" value="<?= $propertydata->getFlooring_type() ?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Total HalfBathrooms</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="number" id="text-input" name="halfbath_total" placeholder="Number Of Half Bathrooms" class="form-control" value="<?= $propertydata->getHalfbath_total() ?>">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Renovated Date</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="date" id="text-input" name="renovated_date" placeholder="Renovated Date" class="form-control" value="<?= $propertydata->getRenovated_date() ?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Total Storeis</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="number" id="text-input" name="stories_total" placeholder="Total Storeis" class="form-control" value="<?= $propertydata->getStories_total() ?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Size Exterior</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="   size_exterior" placeholder="Size Exterior" class="form-control" value="<?= $propertydata->getSize_exterior() ?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Size Interior</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="   size_interior" placeholder="Size Interior" class="form-control" value="<?= $propertydata->getSize_interior() ?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="textarea-input" class=" form-control-label">Street Address</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <textarea name="street_address" id="textarea-input" rows="5" placeholder="Street Address..." class="form-control" ><?= $propertydata->getStreet_address() ?></textarea>
                                        </div>
                                    </div>


                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">City</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="city" placeholder="City" class="form-control" value="<?= $propertydata->getCity() ?>">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="select" class=" form-control-label">Province</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <select name="province" id="select" class="form-control" value="<?= $propertydata->getProvince() ?>">
                                                <option value="0">Please select Province</option>
                                                <option value="Ontario">Ontario</option>
                                                <option value="Quebec">Quebec</option>
                                                <option value="Alberta">Alberta</option>
                                                <option value="British Columbia">British Columbia</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Postal Code</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="postalcode" placeholder="Postal Code" class="form-control" value="<?= $propertydata->getPostalcode() ?>">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="select" class=" form-control-label">Country</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                           <input type="text" name="country" class="form-control" value="Canada" readonly>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Community Name</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="community_name" placeholder="Community Name" class="form-control" value="<?= $propertydata->getCommunity_name() ?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Neighbourhood</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="neighbourhood" placeholder="Neighbourhood" class="form-control" value="<?= $propertydata->getNeighbourhood() ?>" >
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Sub Division</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="text-input" name="subdivision" placeholder="Sub Division" class="form-control" value="<?= $propertydata->getSubdivision() ?>">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                        </div>
                                    </div>



                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="select" class=" form-control-label">Status</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <select name="status" id="select" class="form-control" >
                                                <option value="5">Please select Status</option>
                                               <option value="1" <?= ($propertydata->getStatus() == '1') ? 'selected="selected"' : ''; ?>>0</option>
                                                     <option value="2" <?= ($propertydata->getStatus() == '2') ? 'selected="selected"' : ''; ?>>1</option>
                                            </select>
                                        </div>
                                    </div>
                                                               </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm" name="update">
                                    <i class="fa fa-dot-circle-o"></i> Update Property
                                </button>

                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>

                                <?php if (!$error && $status && $status == "true") : ?>
                                    <div class="alert alert-success mt-2" role="alert">
                                        The property has been insert successfully.
                                    </div>
                                <?php elseif ($error && $error == 3001) : ?>
                                    <div class="alert alert-danger mt-2" role="alert">
                                        Something went wrong.
                                    </div>
                                <?php endif; ?>


                            </div>
                        </div>

                    </div>
                    

                    
                </div>

            </div>

        </div>

    </div>

</div>




</div>

</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="page-container">

                
                  <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                                                   </div>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Room data table</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-right">
                                            <a href="admin-addNewRoom.php?id=<?= $_GET['id'] ?>" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="collapse" >
                                            
                                                        <b>Add Room</b></a>&nbsp;
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                <?php if(isset($_SESSION['msg'])): ?>
                                        <div class="alert alert-<?php echo $_SESSION['msgType'] ?>">
                    <strong><?php echo $_SESSION['msgType'] ?>!</strong> <?= $_SESSION['msg']; ?>
                </div>
            <?php endif; ?>
                 
                        <div class="row m-t-30">
                            <div class="col-md-12">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                                    <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                               <th>ID</th>
                                                <th>type</th>
                                                <th>width</th>
                                                <th>Length</th>
                                                <th>Dimension</th>
                                                <th>Custom Room</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($room_db->getAllRoomsOfProperty($_GET['id']) as $room) :?>
                                            <tr>
                                                <td><?= $room->getRoom_id() ?></td>
                                               
                                                 <td> <?= $room->getType() ?></td>   
                                                <td><?= $room->getWidth() ?></td>
                                                <td ><?= $room->getLength() ?></td>
                                               
                                                <td><?= $room->getDimension() ?></td>
                                                 <td><?= $room->getCustom_room() ?></td>   
                                                

                                                <td>
                                                    <div class="table-data-feature">
                                                        <a href="admin-updateRoom.php?id=<?= $room->getTbl_property_id() ?>" class="btn btn-sm btn-warning" data-toggle="collapse" data-target="#editRoom">
                                                        <b>Edit</b></a>&nbsp;

                                                        <button onclick="del_Room(<?= $room->getRoom_id() ?> , '<?= $room->getDimension() ?>');" class="btn btn-sm btn-danger"><b>Delete</b></button>
                                                </div>
                                                </td>

                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE-->
                            </div>
                        </div>
                                 </div>

                
                  <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                                                   </div>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Photos data table</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-right">
                                            <a href="admin-addNewPhoto.php?id=<?= $_GET['id'] ?> " class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="collapse" >
                                            
                                                        <b>Add Photos</b></a>&nbsp;
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                <?php if(isset($_SESSION['msg'])): ?>
                                        <div class="alert alert-<?php echo $_SESSION['msgType'] ?>">
                    <strong><?php echo $_SESSION['msgType'] ?>!</strong> <?= $_SESSION['msg']; ?>
                </div>
            <?php endif; ?>
               

                        
                        <div class="row m-t-30">
                            <div class="col-md-12">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                                    <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                               <th>Photo Id(details Id)</th>
                                                
                                                <th>Description</th>
                                                <th>Photos</th>
                                                <th>Last Updated</th>
                                               
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($photo_db->getAllPhotosOfProperty($_GET['id']) as $photo) :?>
                                            <tr>
                                               <td><?= $photo->getDetails_id() ?></td>
                                                
                                                <td><?= $photo->getDescription() ?></td>
                                                <td ><div class="row form-group">
                                                <div class="col col-md-3">
                                                    
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <!-- <input type="file" id="file-input" name="photos" class="form-control-file" > -->
                                                    <img src="../assets/rooms/<?= $photo->getPhotos() ?> " width="193" height="130">
                                                </div>
                                            </div></td>
                                                <td><?= $photo->getLast_updated() ?></td>
                                                

                                                
                                                <td>
                                                    <div class="table-data-feature">
                                                   <button onclick="del_Photo(<?= $photo->getDetails_id() ?> , '<?= $photo->getDescription() ?>');" class="btn btn-sm btn-danger"><b>Delete</b></button></a>&nbsp;
                                                    </div>
                                                    </td>

                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE-->
                            </div>
                        </div>

 </div>
                </div>
            </div>
        </div>
</div>



</div>
<script src="../realestate/assets/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="../realestate/assets/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="../realestate/assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="../realestate/assets/vendor/slick/slick.min.js">
</script>
<script src="../realestate/assets/vendor/wow/wow.min.js"></script>
<script src="../realestate/assets/vendor/animsition/animsition.min.js"></script>
<script src="../realestate/assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="../realestate/assets/vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="../realestate/assets/vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="../realestate/assets/vendor/circle-progress/circle-progress.min.js"></script>
<script src="../realestate/assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="../realestate/assets/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="../realestate/assets/vendor/select2/select2.min.js">
</script>

<!-- Main JS-->
<script src="../assets/js/main.js"></script>

<script src="../assets/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="../assets/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="../assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="../assets/vendor/slick/slick.min.js">
</script>
<script src="../assets/vendor/wow/wow.min.js"></script>
<script src="../assets/vendor/animsition/animsition.min.js"></script>
<script src="../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="../assets/vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="../assets/vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="../assets/vendor/circle-progress/circle-progress.min.js"></script>
<script src="../assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="../assets/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="../assets/vendor/select2/select2.min.js">
</script>
<!-- Main JS-->
<script src="../assets/js/main.js"></script>

<?php
include "admin-footer.php";


    if (isset($_POST['update'])) {
        $pc->updateProperty($_POST);
    }
     if (isset($_POST['updateRoom'])) {
        $rc->updateRoom($_POST);
    }

    if (isset($_POST['updatePhoto'])) {
        $photocon->updatePhoto($_POST);
    }

?>
<script>
    function del_Room(id, dimension){
        if(confirm("Are you sure want to delete the room with dimension of : " + dimension +"?")){
            document.location.href = 'admin-properties.php?id='+id;
        }
    }
</script>

<script>
    function del_Photo(id, description){
        if(confirm("Are you sure want to delete the photo with description of : " + description +"?")){
            document.location.href = 'admin-properties.php?id='+id;
        }
    }
</script>

