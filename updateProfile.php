<?php
include "head.php";
include "Container.php";
include "header.php";
include "./controller/UserController.php";
include "./controller/loginController.php";
include "./controller/PropertyController.php";
$lc = new loginController();
$lc->checkLogin();
$uc = new UserController();
if ($uc->checkUserFromUserSide(true)) {
    $lc->redirectToAdmin();
}
$prop_db = new PropertyDBManager();
$user = $uc->getCurrentUserData();
$status = (isset($_GET['success'])) ? $_GET['success'] : null;
$error = (isset($_GET['error'])) ? $_GET['error'] : null;
?>

    <div class="super_container">

        <!-- Header -->
        <div class="home">
            <div class="parallax_background parallax-window" data-parallax="scroll"
                 style="background-image:url(assets/images/elements.jpg)" data-speed="0.8"></div>
            <div class="home_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="home_content d-flex flex-row align-items-end justify-content-start">
                                <div class="home_title">Update Profile</div>
                                <div class="breadcrumbs ml-auto">
                                    <ul>
                                        <li><a href="index.htmo">Home</a></li>
                                        <li>Update Profile</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <form class="login100-form validate-form" id="edit" action="" method="post">
					<span class="login100-form-title p-b-34">
						Update Profile
					</span>
                        <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20"
                             data-validate="Type user name">
                            <input id="fullname" class="input100" type="text" name="fullname" placeholder="Full name"
                                   value="<?= $user->getFullname(); ?>" required>
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20"
                             data-validate="Type user name">
                            <input id="username" class="input100" type="text" name="username" maxlength="20"
                                   placeholder="User name" value="<?= $user->getUsername() ?>"
                                   style="cursor: not-allowed;" readonly required>
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20"
                             data-validate="Type user name">
                            <input id="email" class="input100" type="email" name="email" placeholder="Email"
                                   value="<?= $user->getEmail() ?>" required>
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type dob">
                            <input class="input100" id="dob" type="date" name="dob" placeholder="Date Of Birth"
                                   value="<?= $user->getDob() ?>" required>
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20"
                             data-validate="Type password">
                            <input class="input100" id="phone" type="number" name="phone" placeholder="Phone Number"
                                   value="<?= $user->getPhone() ?>" required>
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type address">
                            <input id="address" class="input100" type="text" name="address" placeholder="Address"
                                   value="<?= $user->getAddress() ?>" required>
                            <span class="focus-input100"></span>
                        </div>

                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn" name="editUser" type="submit">
                                Update Profile
                            </button>

                            <?php if (!$error && $status && $status == "true") : ?>
                                <div class="alert alert-success mt-2" role="alert">
                                    Your profile has been updated successfully.
                                </div>
                            <?php elseif ($error && $error == 1004) : ?>
                                <div class="alert alert-danger mt-2" role="alert">
                                    Error in updation process.
                                </div>
                            <?php elseif ($error && $error == 1003) : ?>
                                <div class="alert alert-danger mt-2" role="alert">
                                    All fields are required.
                                </div>
                            <?php elseif ($error && $error == 1001) : ?>
                                <div class="alert alert-danger mt-2" role="alert">
                                    Invalid email address.
                                </div>
                            <?php endif; ?>
                        </div>
                    </form>
                    <div class="login100-more"
                         style="background-image: url('assets/login_css/images/bg-01.jpg');"></div>
                </div>
            </div>
            <hr style="border: 10px solid #3F6FCE; display: block;">

            <div class="properties">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="section_title">25 Properties found</div>
                            <div class="section_subtitle">Search your dream home</div>
                        </div>
                        <div class="pull-right">
                            <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Post Property</button>
                        </div>
                    </div>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Post Property</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Property Name</label>
                                        <input type="text" class="form-control" required="required">
                                    </div>
                                    <div class="form-group">
                                        <div class="clearfix">
                                            <label>Room Space</label>
                                            <a href="#" class="pull-right text-muted"><small>Forgot?</small></a>
                                        </div>

                                        <input type="password" class="form-control" required="required">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row properties_row">

                        <!-- Property -->
                        <?php foreach ($prop_db->getUserProperties($_GET['id']) as $properties) :?>
                        <div class="col-xl-4 col-lg-6 property_col">
                            <div class="property">
                                <div class="property_image">
                                    <img src="assets/images/property_1.jpg" alt="">
                                    <div class="tag_featured property_tag"><a href="#">Featured</a></div>
                                </div>
                                <div class="property_body text-center">
                                    <div class="property_location"><?= $properties->getCity() ?></div>
                                    <div class="property_title"><a href=""><?= $properties->getFeatures() ?></a></div>
                                    <div class="property_price">$ <?= $properties->getPrice() ?></div>
                                </div>
                                <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                                    <div>
                                        <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                        <span><?= $properties->getSize_exterior() ?> Ftsq </span></div>
                                    <div>
                                        <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                        <span><?= $properties->getBedroom_total() ?> Bedrooms</span></div>
                                    <div>
                                        <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                        <span><?= $properties->getBathroom_total() ?> Bathrooms</span></div>
                                </div>
                            </div>
                        </div>
<?php endforeach; ?>

                        <!-- Property -->
                        <div class="col-xl-4 col-lg-6 property_col">
                            <div class="property">
                                <div class="property_image">
                                    <img src="assets/images/property_4.jpg" alt="">
                                    <div class="tag_new property_tag"><a href="#">New</a></div>
                                </div>
                                <div class="property_body text-center">
                                    <div class="property_location">Miami</div>
                                    <div class="property_title"><a href="">Sea view property</a></div>
                                    <div class="property_price">$ 1. 234 981</div>
                                </div>
                                <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                                    <div>
                                        <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                        <span>650 Ftsq</span></div>
                                    <div>
                                        <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                        <span>3 Bedrooms</span></div>
                                    <div>
                                        <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                        <span>3 Bathrooms</span></div>
                                </div>
                            </div>
                        </div>

                        <!-- Property -->
                        <div class="col-xl-4 col-lg-6 property_col">
                            <div class="property">
                                <div class="property_image">
                                    <img src="assets/images/property_5.jpg" alt="">
                                </div>
                                <div class="property_body text-center">
                                    <div class="property_location">New York</div>
                                    <div class="property_title"><a href="">Sea view property</a></div>
                                    <div class="property_price">$ 1. 234 981</div>
                                </div>
                                <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                                    <div>
                                        <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                        <span>650 Ftsq</span></div>
                                    <div>
                                        <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                        <span>3 Bedrooms</span></div>
                                    <div>
                                        <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                        <span>3 Bathrooms</span></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="pagination">
                                <ul>
                                    <li class="active"><a href="#">01.</a></li>
                                    <li><a href="#">02.</a></li>
                                    <li><a href="#">03.</a></li>
                                    <li><a href="#">04.</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--    End Property DIV        -->
            <div>
<!--                <div id="myModal" class="modal fade" role="dialog">-->
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Post Property</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Property Name</label>
                                    <input type="text" class="form-control" required="required">
                                </div>
                                <div class="form-group">
                                    <div class="clearfix">
                                        <label>Room Space</label>
                                        <a href="#" class="pull-right text-muted"><small>Forgot?</small></a>
                                    </div>

                                    <input type="password" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
<!--                </div>-->
            </div>
        </div>
    </div>

    <script src="assets/login_css/images/icons/favicon.ico"></script>
    <script src="assets/login_css/vendor/bootstrap/css/bootstrap.min.css"></script>
    <script src="assets/login_css/fonts/font-awesome-4.7.0/css/font-awesome.min.css"></script>
    <script src="assets/login_css/fonts/Linearicons-Free-v1.0.0/icon-font.min.css"></script>
    <script src="assets/login_css/vendor/animate/animate.css"></script>
    <script src="assets/login_css/vendor/css-hamburgers/hamburgers.min.css"></script>
    <script src="assets/login_css/vendor/animsition/css/animsition.min.css"></script>
    <script src="assets/login_css/vendor/select2/select2.min.css"></script>
    <script src="assets/login_css/vendor/daterangepicker/daterangepicker.css"></script>
    <script src="assets/login_css/css/util.css"></script>
    <script src="assets/login_css/css/main.css"></script>

<?php
include "footer.php";

if (isset($_POST['editUser'])) {
    $uc->editUser($_POST);
}
?>