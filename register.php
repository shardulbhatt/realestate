<?php
	include "head.php";
	include "Container.php";
	include "header.php";
	include "./controller/UserController.php";
	$status = (isset($_GET['success'])) ? $_GET['success'] : null ;  
	$error = (isset($_GET['error'])) ? $_GET['error'] : null ;  
?>

<div class="super_container">

	<!-- Header -->
<div class="home">
		<div class="parallax_background parallax-window" data-parallax="scroll" style="background-image:url(assets/images/elements.jpg)" data-speed="0.8"></div>
		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content d-flex flex-row align-items-end justify-content-start">
							<div class="home_title">Registration</div>
							<div class="breadcrumbs ml-auto">
								<ul>
									<li><a href="index.htmo">Home</a></li>
									<li>Registration</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
			<?php if(isset($_SESSION['msg'])): ?>
				<div class="alert alert-<?php echo $_SESSION['msgType'] ?>">
					<strong><?php echo $_SESSION['msgType'] ?>!</strong> <?= $_SESSION['msg']; ?>
				</div>
			<?php endif; ?>
				<form class="login100-form validate-form" action="" method="post">
				<input type="hidden" name="action" value="add">
					<span class="login100-form-title p-b-34">
						Register
					</span>
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input id="fname" class="input100" type="text" name="fname" placeholder="First name" required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input id="lname" class="input100" type="text" name="lname" placeholder="Last Name" required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input id="username" class="input100" type="text" name="username" maxlength="20" 
						placeholder="User name" required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input id="email" class="input100" type="email" name="email" placeholder="Email" required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input class="input100"  id="password" type="password" name="password" maxlength="32" placeholder="Password" required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type condirmpassword">
						<input class="input100" id="cpassword" type="password" name="confpass" maxlength="32" placeholder="Conirm Password" required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type dob" >
						<input class="input100" id="dob" type="date" name="dob" placeholder="Date Of Birth" required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input class="input100" id="phone" type="number" name="phone" placeholder="Phone Number" required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type address">
						<input id="address" class="input100" type="text" name="address" placeholder="Address" required>
						<span class="focus-input100"></span>
					</div>
					<!--<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type country">
						<input id="country" class="input100" type="country" name="country" placeholder="Country">
						<span class="focus-input100"></span>
					</div>-->
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" name="addUser" type="submit">
							Sign up
						</button>
					
						<?php if(!$error && $status && $status == "true"): ?>
							<div class="alert alert-success mt-2" role="alert">
								You have been register successfully.
							</div>
						<?php elseif ($error && $error == 1001): ?>
							<div class="alert alert-danger mt-2" role="alert">
								Invalid email address.
							</div>
						<?php elseif ($error && $error == 1000): ?>
							<div class="alert alert-danger mt-2" role="alert">
								This username is already exist.
							</div>
						<?php elseif ($error && $error == 1002): ?>
							<div class="alert alert-danger mt-2" role="alert">
								Password and Confirm Password doesn't match.
							</div>
						<?php elseif ($error && $error == 1006): ?>
							<div class="alert alert-danger mt-2" role="alert">
								Invalid length of phone Number, it should be 10.
							</div>
						<?php elseif ($error && $error == 1005): ?>
							<div class="alert alert-danger mt-2" role="alert">
								Invalid type of phone number.
							</div>

						<?php elseif ($error && $error == 1003): ?>
							<div class="alert alert-danger mt-2" role="alert">
								All fields are required.
							</div>
						<?php elseif ($error && $error == 1004): ?>
							<div class="alert alert-danger mt-2" role="alert">
								Error in register process.
							</div>
						<?php endif; ?>

					</div>

				<!--	<div class="w-full text-center p-t-27 p-b-239">
						<span class="txt1">
							Forgot
						</span>

						<a href="#" class="txt2">
							User name / password?
						</a>
					</div>

					<div class="w-full text-center">
						<a href="register.php" class="txt3">
							Sign Up
						</a>
					</div>-->
				</form>

				<div class="login100-more" style="background-image: url('assets/login_css/images/bg-01.jpg');"></div>
			</div>
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>
	
	</div>
	
	
	<script src="assets/login_css/images/icons/favicon.ico"></script>
	<script src = "assets/login_css/vendor/bootstrap/css/bootstrap.min.css"></script>
	<script src = "assets/login_css/fonts/font-awesome-4.7.0/css/font-awesome.min.css"></script>
	<script src = "assets/login_css/fonts/Linearicons-Free-v1.0.0/icon-font.min.css"></script>
	<script src = "assets/login_css/vendor/animate/animate.css"></script>
	<script src = "assets/login_css/vendor/css-hamburgers/hamburgers.min.css"></script>
	<script src = "assets/login_css/vendor/animsition/css/animsition.min.css"></script>
	<script src = "assets/login_css/vendor/select2/select2.min.css"></script>
	<script src = "assets/login_css/vendor/daterangepicker/daterangepicker.css"></script>
	<script src = "assets/login_css/css/util.css"></script>
	<script src = "assets/login_css/css/main.css"></script>
	
<?php
	include "footer.php";
	if(isset($_POST['addUser']))
    {
		$uc = new UserController();
		$uc->addUser($_POST);
    }
?>