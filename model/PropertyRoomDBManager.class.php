<?php

class PropertyRoomDBManager extends DBManager {
	protected $db;

	/**
	 * Student constructor.
	 */
	public function __construct() {
		$this->db = DBManager::Instance()->getDb();
	}

	public function getAllRooms() {
		$rooms = array();
		$query    = $this->db->query( "SELECT * FROM tbl_property_rooms ORDER BY room_id DESC" ); //whenever you want to select everything from your table
		$result   = $query->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $result as $r ) {
			$rooms[] = new PropertyRoomBean( $r );
		}

		return $rooms;
	}

	public function getAllRoomsOfProperty($tbl_property_id) {
		$rooms = array();
		$query    = $this->db->prepare( "SELECT * FROM tbl_property_rooms WHERE tbl_property_id = ?"); //whenever you want to select everything from your table
		$query->execute( array( $tbl_property_id ) );
		$result   = $query->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $result as $r ) {
			$rooms[] = new PropertyRoomBean( $r );
		}

		return $rooms;
	}



	public function getSingleRoom( $tbl_property_id ) {

		$query = $this->db->prepare( "SELECT * FROM tbl_property_rooms WHERE tbl_property_id = ?" );
		 //whenever you want to select a single record from your table
		$query->execute( array( $tbl_property_id ) );
		$result = $query->fetch( PDO::FETCH_ASSOC );

		return new PropertyRoomBean( $result );
	}

	public function addRoom( $room ) {
		$query = $this->db->prepare( "INSERT INTO tbl_property_rooms VALUES (DEFAULT, :listing_id, :type, :width, :length, :level, :dimension, :custom_room, :tbl_property_id ,DEFAULT)" );
		$query->execute( array(
			"listing_id"    => $room->getListing_id(),
			"type"    => $room->getType(),
			"width"      => $room->getWidth(),
			"length"   => $room->getLength(),
			"level" => $room->getLevel(),
			"dimension"  => $room->getDimension(),
			"custom_room" => $room->getCustom_room(),
			"tbl_property_id" => $room->getTbl_property_id()
		) );

		return $this->db->lastInsertId();
	}

	public function editRoom( $room ) {
		$query = $this->db->prepare( "UPDATE tbl_property_rooms
 SET  type = :type, width = :width, length = :length, level = :level, dimension = :dimension, custom_room = :custom_room, status = :status WHERE room_id = :room_id" );

		$query->execute( array(
			"room_id"       => $room->getRoom_id(),
			"type"    => $room->getType(),
			"width"      => $room->getWidth(),
			"length"   => $room->getLength(),
			"level" => $room->getLevel(),
			"dimension"  => $room->getDimension(),
			"custom_room" => $room->getCustom_room(),
			"status"   => $room->getStatus()
		) );
	}
	public function deleteRoom($room_id){
		$query = $this->db->prepare("DELETE FROM tbl_property_rooms WHERE room_id = :room_id");
		$query->execute(array("room_id" => $room_id));
	}

	public function banRoom($room_id){
		$query = $this->db->prepare("UPDATE tbl_property_rooms SET status = 0 WHERE room_id = :room_id");
		$query->execute(array("room_id" => $room_id));
	}
}
?>