<?php

class PropertyPhotosDBManager extends DBManager {
	protected $db;

	/**
	 * Student constructor.
	 */
	public function __construct() {
		$this->db = DBManager::Instance()->getDb();
	}

	public function getAllPhotos() {
		$photos = array();
		$query    = $this->db->query( "SELECT * FROM tbl_property_photos ORDER BY details_id DESC" ); //whenever you want to select everything from your table
		$result   = $query->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $result as $r ) {
			$photos[] = new PropertyPhotosBean( $r );
		}

		return $photos;
	}

	public function getAllPhotosOfProperty($tbl_property_id) {
		$photos = array();
		$query    = $this->db->prepare( "SELECT * FROM tbl_property_photos WHERE tbl_property_id = ?"); //whenever you want to select everything from your table
		$query->execute( array( $tbl_property_id ) );
		$result   = $query->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $result as $r ) {
			$photos[] = new PropertyPhotosBean( $r );
		}

		return $photos;
	}

	public function getSinglePhoto( $tbl_property_id ) {
		$query = $this->db->prepare( "SELECT * FROM tbl_property_photos WHERE tbl_property_id = ?" ); //whenever you want to select a single record from your table
		$query->execute( array( $tbl_property_id ) );
		$result = $query->fetch( PDO::FETCH_ASSOC );

		return new PropertyPhotosBean( $result );
	}

	public function addPhoto( $photo ) {
		$photo->setSequence_id(rand());
		try
		{
			$query = $this->db->prepare( "INSERT INTO tbl_property_photos VALUES (DEFAULT, :listing_id, :sequence_id, :description, :photos, :last_updated, :photos_last_updated, :custom_photo,  :tbl_property_id ,DEFAULT)" );

			$query->execute( array(
				"listing_id"    => $photo->getListing_id(),
				"sequence_id"    => $photo->getSequence_id(),
				"description" =>$photo->getDescription(),
				"photos"      => $photo->getPhotos(),
				"last_updated"   => date("Y-m-d H:m:s", strtotime($photo->getLast_updated())),
				"photos_last_updated" => $photo->getPhotos_last_updated(),
				"custom_photo"  => $photo->getCustom_photo(),
				"tbl_property_id" =>$photo->getTbl_property_id()
			) );

			return $this->db->lastInsertId();
		}
		catch(Exception $e)
		{
			return null;
		}

	}

	public function editPhoto( $photo ) {
		$query = $this->db->prepare( "UPDATE tbl_property_photos SET listing_id = :listing_id,  description = :description, photos = :photos, last_updated = :last_updated, photos_last_updated = :photos_last_updated, custom_photo = :custom_photo, status = :status WHERE details_id = :details_id" );
		$query->execute( array(
			"details_id"       => $photo->getDetails_id(),
			"listing_id"    => $photo->getListing_id(),
			"description"      => $photo->getDescription(),
			"photos"   => $photo->getPhotos(),
			"last_updated" => $photo->getLast_updated(),
			"photos_last_updated"  => $photo->getPhotos_last_updated(),
			"custom_photo" => $photo->getCustom_photo(),
			"status"   => $photo->getStatus()
		) );
	}

	public function deletePhoto($details_id){
		$query = $this->db->prepare("DELETE FROM tbl_property_photos WHERE details_id = :details_id");
		$query->execute(array("details_id" => $details_id));
	}

public function banPhoto($details_id){
		$query = $this->db->prepare("UPDATE tbl_property_photos SET status = 0 WHERE details_id = :listing_id");
		$query->execute(array("details_id" => $details_id));
	}


}

?>