<?php
		class PropertyPhotosBean
		{
			private $details_id;
			private $listing_id;
			private $sequence_id;
			private $description;
			private $photos;
			private $last_updated;
			private $photos_last_updated;
			private $custom_photo;
			private $tbl_property_id;
			private $status;

			public function __construct($data){

				$this->details_id				=$data['details_id']??null;
				$this->listing_id 				=$data['listing_id'];
				$this->description 				=$data['description'];
				$this->photos 					=$data['photos']?? null;
				$this->last_updated 			=$data['last_updated'];
				$this->photos_last_updated 		=$data['photos_last_updated'];
				$this->custom_photo				=$data['custom_photo'];
				$this->tbl_property_id			=$data['tbl_property_id'];
				$this->status 					=$data['status']??1;
			}	

	public function getDetails_id(){
		return $this->details_id;
	}

	public function setDetails_id($details_id){
		$this->details_id = $details_id;
	}

	public function getListing_id(){
		return $this->listing_id;
	}

	public function setListing_id($listing_id){
		$this->listing_id = $listing_id;
	}

	public function getSequence_id(){
		return $this->sequence_id;
	}

	public function setSequence_id($sequence_id){
		$this->sequence_id = $sequence_id;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function getPhotos(){
		return $this->photos;
	}

	public function setPhotos($photos){
		$this->photos = $photos;
	}

	public function getLast_updated(){
		return $this->last_updated;
	}

	public function setLast_updated($last_updated){
		$this->last_updated = $last_updated;
	}

	public function getPhotos_last_updated(){
		return $this->photos_last_updated;
	}

	public function setPhotos_last_updated($photos_last_updated){
		$this->photos_last_updated = $photos_last_updated;
	}

	public function getCustom_photo(){
		return $this->custom_photo;
	}

	public function setCustom_photo($custom_photo){
		$this->custom_photo = $custom_photo;
	}


	public function getTbl_property_id(){
		return $this->tbl_property_id;
	}

	public function setTbl_property_id($tbl_property_id){
		$this->tbl_property_id = $tbl_property_id;
	}
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}


		}
?>