<?php 

class AlertBean 
{
	private $id;
	private $search_criteria;
	private $status;
	private $tbl_users_id;

	public function __construct($data){

		$this->id =	$data['id'] ??null;
		$this->search_criteria = $data['search_criteria'];
		$this->status = $data['status']??1;
		$this->tbl_users_id = $data['tbl_users_id'];
	}

		public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getSearch_criteria(){
		return $this->search_criteria;
	}

	public function setSearch_criteria($search_criteria){
		$this->search_criteria = $search_criteria;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}

	public function getTbl_users_id(){
		return $this->tbl_users_id;
	}

	public function setTbl_users_id($tbl_users_id){
		$this->tbl_users_id = $tbl_users_id;
	}


}

?>