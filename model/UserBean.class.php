<?php
	class UserBean{
		private $id ;
		private $username;
		private $password;
		private $fullname;
		private $dob;
		private $email;
		private $phone;
		private $address;
		private $level ;
		private $status ;
		private $addPermission;
		private $updatePermission;
		private $deletePermission;

		public function __construct($data){
		$this->id =			$data['id'] ??null;
		$this->username =	$data['username'];
		$this->password =	$data['password'];
		$this->fullname =	(isset($data['fname']) && isset($data['lname'])) ? $data['fname']." ".$data['lname'] : $data['fullname'];
		$this->dob =		$data['dob'];
		$this->email =		$data['email'];
		$this->phone =		$data['phone'];
		$this->address =	$data['address'];
		$this->level =		$data['level'] ??1;
		$this->status =		$data['status'] ?? 1;
		$this->addPermission = $data['addPermission'] ?? 1;
		$this->updatePermission = $data['updatePermission'] ?? 1;
		$this->deletePermission = $data['deletePermission'] ?? 1;
	}
			
		

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getUsername(){
		return $this->username;
	}

	public function setUsername($username){
		$this->username = $username;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setPassword($password){
		$this->password = $password;
	}

	public function getFullname(){
		return $this->fullname;
	}

	public function setFullname($fullname){
		$this->fullname = $fullname;
	}

	public function getDob(){
		return $this->dob;
	}

	public function setDob($dob){
		$this->dob = $dob;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function getPhone(){
		return $this->phone;
	}

	public function setPhone($phone){
		$this->phone = $phone;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setAddress($address){
		$this->address = $address;
	}

	public function getLevel(){
		return $this->level;
	}

	public function setLevel($level){
		$this->level = $level;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}
		/*GETTERS AND SETTERS*/

	    /**
	    * Gets the value of student_id
	    * @return mixed
	    */



	    public function getAddPermission()
	    {
	    	return $this->addPermission;
	    }
	    public function setAddPermission($addPermission)
	    {
	    	$this->addPermission =$addPermission;
	    }

	    public function getUpdatePermission()
	    {
	    	return $this->updatePermission;
	    }
	    public function setUpdatePermission($updatePermission)
	    {
	    	$this->updatePermission =$updatePermission;	
	    }

	    public function getDeletePermission()
	    {
	    	return $this->deletePermission;
	    }
	    public function setDeletePermission()
	    {
	    	$this->deletePermission =$deletePermission;
	    }



	    
    }


 ?>