<?php

class PropertyDBManager extends DBManager {
	protected $db;

	/**
	 * Student constructor.
	 */
	public function __construct() {
		$this->db = DBManager::Instance()->getDb();
	}

	public function getAllProperties() {
		$properties = array();
		$query    = $this->db->query( "SELECT * FROM tbl_property ORDER BY property_id DESC" ); //whenever you want to select everything from your table
		$result   = $query->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $result as $r ) {
			$properties[] = new PropertyBean( $r );
		}

		return $properties;
	}

	public function getSingleProperty( $property_id ) {
		
		$query = $this->db->prepare( "SELECT * FROM tbl_property WHERE property_id = ?" );
		 //whenever you want to select a single record from your table
		$query->execute( array( $property_id ) );
		$result = $query->fetch( PDO::FETCH_ASSOC );
		
		return new PropertyBean( $result );
	}

	public function addProperty( $property ) {
		$query = $this->db->prepare( "INSERT INTO tbl_property VALUES (DEFAULT, :tbl_users_id, :last_updated, :features, :parking_space_total, :price, :property_type, :public_remarks, :structure, :bathroom_total,:bedroom_total,:architectural_style,:constructed_date, :exterior_finish, :flooring_type,:halfbath_total,:renovated_date,:stories_total, :size_exterior, :size_interior,:street_address, :city,:province,:postalcode, :country, :community_name, :neighbourhood, :subdivision,DEFAULT)" );
		$query->execute( array(
			"tbl_users_id"    => $property->getTbl_users_id(),
			"last_updated"    => date("Y-m-d H:m:s", strtotime( $property->getLast_updated())),
			"features"      => $property->getFeatures(),
			"parking_space_total"   => $property->getParking_space_total(),
			"price" => $property->getPrice(),
			"property_type"  => $property->getProperty_type(),
			"public_remarks" => $property->getPublic_remarks(),
			"structure" =>$property->getStructure(),
			"bathroom_total"=>$property->getBathroom_total(),
			"bedroom_total"=>$property->getBedroom_total(),
			"architectural_style"=>$property->getArchitectural_style(),
			"constructed_date"=>date("Y-m-d H:m:s", strtotime( $property->getConstructed_date())),
			"exterior_finish"=>$property->getExterior_finish(),
			"flooring_type"=>$property->getFlooring_type(),
			"halfbath_total"=>$property->getHalfbath_total(),
			"renovated_date"=>date("Y-m-d H:m:s", strtotime( $property->getRenovated_date())),
			"stories_total"=>$property->getStories_total(),
			"size_exterior"=>$property->getSize_exterior(),
			"size_interior"=>$property->getSize_interior(),
			"street_address"=>$property->getStreet_address(),
			"city"=>$property->getCity(),
			"province"=>$property->getProvince(),
			"postalcode"=>$property->getPostalcode(),
			"country"=>$property->getCountry(),
			"community_name"=>$property->getCommunity_name(),
			"neighbourhood"=>$property->getNeighbourhood(),
			"subdivision"=>$property->getSubdivision()
		) );
		
		return $this->db->lastInsertId();
	}

	public function editProperty( $property ) {
		$query = $this->db->prepare( "UPDATE tbl_property SET tbl_users_id = :tbl_users_id,
		 last_updated = :last_updated,
		 features= :features,
		 parking_space_total= :parking_space_total, 
		 price=:price,
		 property_type= :property_type,
		 public_remarks= :public_remarks,
		 structure= :structure,
		  bathroom_total= :bathroom_total,
		  bedroom_total=:bedroom_total,
		  architectural_style=:architectural_style,
		  constructed_date=:constructed_date,
		  exterior_finish= :exterior_finish, 
		  flooring_type=:flooring_type,
		  halfbath_total=:halfbath_total,
		  renovated_date=:renovated_date,
		  stories_total=:stories_total, 
		  size_exterior= :size_exterior, 
		  size_interior=:size_interior,
		  street_address=:street_address,
		   city=:city,
		   province=:province,
		   postalcode=:postalcode, 
		   country=:country,
		   community_name= :community_name, 
		   neighbourhood= :neighbourhood,
		    subdivision=:subdivision, 
		    status = :status WHERE property_id = :property_id" );
		$query->execute( array(
			"property_id"       => $property->getProperty_id(),
			"tbl_users_id"    => $property->getTbl_users_id(),
			"last_updated"    => $property->getLast_updated(),
			"features"      => $property->getFeatures(),
			"parking_space_total"   => $property->getParking_space_total(),
			"price" => $property->getPrice(),
			"property_type"  => $property->getProperty_type(),
			"public_remarks" => $property->getPublic_remarks(),
			"structure" =>$property->getStructure(),
			"bathroom_total"=>$property->getBathroom_total(),
			"bedroom_total"=>$property->getBedroom_total(),
			"architectural_style"=>$property->getArchitectural_style(),
			"constructed_date"=>$property->getConstructed_date(),
			"exterior_finish"=>$property->getExterior_finish(),
			"flooring_type"=>$property->getFlooring_type(),
			"halfbath_total"=>$property->getHalfbath_total(),
			"renovated_date"=>$property->getRenovated_date(),
			"stories_total"=>$property->getStories_total(),
			"size_exterior"=>$property->getSize_exterior(),
			"size_interior"=>$property->getSize_interior(),
			"street_address"=>$property->getStreet_address(),
			"city"=>$property->getCity(),
			"province"=>$property->getProvince(),
			"postalcode"=>$property->getPostalcode(),
			"country"=>$property->getCountry(),
			"community_name"=>$property->getCommunity_name(),
			"neighbourhood"=>$property->getNeighbourhood(),
			"subdivision"=>$property->getSubdivision(),
			"status"   => $property->getStatus()
		) );
	}

	public function deleteProperty($property_id){
		$query = $this->db->prepare("DELETE FROM tbl_property WHERE property_id = :property_id");
		$query->execute(array("property_id" => $property_id));
	}

	public function banProperty($property_id){
		$query = $this->db->prepare("UPDATE tbl_property SET status = 0 WHERE property_id = :property_id");
		$query->execute(array("property_id" => $property_id));
	}



	public function getUserProperties($tbl_users_id) {
		$property = array();
		$query    = $this->db->prepare( "SELECT * FROM tbl_property WHERE tbl_users_id = ?"); //whenever you want to select everything from your table
		$query->execute( array( $tbl_users_id ) );
		$result   = $query->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $result as $r ) {
			$property[] = new PropertyBean( $r );
		}

		return $property;
	}

	/*public function getUserProperties( $tbl_users_id ) {
		
		$query = $this->db->prepare( "SELECT * FROM tbl_property WHERE tbl_users_id = ?" );
		 //whenever you want to select a single record from your table
		$query->execute( array( $tbl_users_id ) );
		$result = $query->fetch( PDO::FETCH_ASSOC );
		
		return new PropertyBean( $result );
	}
*/


}
?>