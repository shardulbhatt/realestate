<? php 

class VisitBean
{
	private $id;
	private $tbl_users_id;
	private $tbl_property_id;
	private $user_coments;
	private $status;

	public function __construct($data){

		$this->id =	$data['id'] ??null;
		$this->tbl_users_id = $data['tbl_users_id'];
		$this->tbl_property_id = $data['tbl_property_id'];
		$this->user_coments = $data['user_coments'];
		$this->status = $data['status'];
	}

		public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getTbl_users_id(){
		return $this->tbl_users_id;
	}

	public function setTbl_users_id($tbl_users_id){
		$this->tbl_users_id = $tbl_users_id;
	}

	public function getTbl_property_id(){
		return $this->tbl_property_id;
	}

	public function setTbl_property_id($tbl_property_id){
		$this->tbl_property_id = $tbl_property_id;
	}

	public function getUser_coments(){
		return $this->user_coments;
	}

	public function setUser_coments($user_coments){
		$this->user_coments = $user_coments;
	}
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}
}

?>