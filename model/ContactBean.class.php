<?php

/**
* 
*/
class ContactBean
{
	
	private $id;
	private $name;
	private $email;
	private $subject;
	private $message;
	private $status;

	public function __construct($data){

		$this->id =	$data['id'] ??null;
		$this->name = $data['name'];
		$this->email = $data['email'];
		$this->subject = $data['subject'];
		$this->message = $data['message'];
		$this->status = $data['status']??1;
	}

		public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function getSubject(){
		return $this->subject;
	}

	public function setSubject($subject){
		$this->subject = $subject;
	}

	public function getMessage(){
		return $this->message;
	}

	public function setMessage($message){
		$this->message = $message;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}


}

?>