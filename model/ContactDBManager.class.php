<?php

class ContactDBManager extends DBManager {
	protected $db;

	/**
	 * Student constructor.
	 */
	public function __construct() {
		$this->db = DBManager::Instance()->getDb();
	}

	public function getAllContacts() {
		$contacts = array();
		$query    = $this->db->query( "SELECT * FROM tbl_contact ORDER BY id DESC" ); //whenever you want to select everything from your table
		$result   = $query->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $result as $r ) {
			$contacts[] = new ContactBean( $r );
		}

		return $contacts;
	}

	public function addContact( $contact ) {
		var_dump($contact);
		$query = $this->db->prepare( "INSERT INTO tbl_contact VALUES (DEFAULT, :name, :email, :subject, :message,DEFAULT)" );
		$query->execute( array(
			"name"    => $contact->getName(),
			"email"    => $contact->getEmail(),
			"subject"      => $contact->getSubject(),
			"message" => $contact->getMessage()
		) );
	}



}

?>