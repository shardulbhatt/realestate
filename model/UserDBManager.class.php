<?php

class UserDBManager extends DBManager {
	protected $db;

	/**
	 * Student constructor.
	 */
	public function __construct() {
		$this->db = DBManager::Instance()->getDb();
	}

	public function getAllUsers() {
		$users = array();
		$query    = $this->db->query( "SELECT * FROM tbl_users ORDER BY id DESC" ); //whenever you want to select everything from your table
		$result   = $query->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $result as $r ) {
			$users[] = new UserBean( $r );
		}

		return $users;
	}

	public function getSingleUser( $id ) {
		$query = $this->db->prepare( "SELECT * FROM tbl_users WHERE id = ?" ); //whenever you want to select a single record from your table
		$query->execute( array( $id ) );
		$result = $query->fetch( PDO::FETCH_ASSOC );

		return new UserBean( $result );
	}

	public function addUser( $user ) {
		
		$query = $this->db->prepare( "INSERT INTO tbl_users VALUES (DEFAULT, :username, :password, :fullname, :dob, :email, :phone, :address, :level ,DEFAULT)" );
		$query->execute( array(
			"username"    => $user->getUsername(),
			"password"    => $user->getPassword(),
			"fullname"      => $user->getFullname(),
			"dob"   => date("Y-m-d H:m:s", strtotime( $user->getDob())),
			"email" => $user->getEmail(),
			"phone"  => $user->getPhone(),
			"address" => $user->getAddress(),
			"level" =>$user->getLevel()
		) );
	}

	/*public function userLogin($username,$password)
	{
		
	}*/



	public function editUser( $user ) {
		$query = $this->db->prepare( "UPDATE tbl_users SET username = :username, password = :password, fullname = :fullname, dob = :dob, email = :email, phone = :phone, address = :address, 
			level = :level ,status = :status WHERE id = :id" );
		$query->execute( array(
			"id"       => $user->getId(),
			"username"    => $user->getUsername(),
			"password"    => $user->getPassword(),
			"fullname"      => $user->getFullname(),
			"dob"   => $user->getDob(),
			"email" => $user->getEmail(),
			"phone"  => $user->getPhone(),
			"address" => $user->getAddress(),
			"level" => $user->getLevel(),
			"status"   => $user->getStatus()
		) );
	}

	public function deleteUser($id){
		try
		{
			$query = $this->db->prepare("DELETE FROM tbl_users WHERE id = :id");
			$query->execute(array("id" => $id));
			return true;
		}
		catch(Exception $e)
		{
			return false;
		}

	}

	public function banUser($id){
		$query = $this->db->prepare("UPDATE tbl_users SET status = 0 WHERE id = :id");
		$query->execute(array("id" => $id));
	}

	public function checkUsernameExistOrNot($uname)
	{
		$query = $this->db->prepare("SELECT COUNT(*) FROM tbl_users WHERE username = ?");
		$query->execute( array( $uname ) );
		$result = $query->fetch( PDO::FETCH_COLUMN );
		return ($result > 0) ? true : false;
	}

	public function checkLoginCredentials($uname,$password)
	{
		$query = $this->db->prepare("SELECT * FROM tbl_users WHERE username = :uname AND password = :password ");
		$query->execute(['uname' => $uname,'password' => $password]);
		$result = $query->fetch( PDO::FETCH_ASSOC );
		return $result;
	}

	public function getSingleUserByUsername( $username ) {
		$query = $this->db->prepare( "SELECT * FROM tbl_users WHERE username = ?" ); 
		$query->execute( array( $username ) );
		$result = $query->fetch( PDO::FETCH_ASSOC );

		return new UserBean( $result );
	}

	public function editUserByUsername( $user ) {
		$query = $this->db->prepare( "UPDATE tbl_users SET fullname = :fullname, dob = :dob, email = :email, phone = :phone, address = :address WHERE username = :username" );
		$query->execute( array(
			"username"    => $user->getUsername(),
			"fullname"    => $user->getFullname(),
			"dob"   	  => $user->getDob(),
			"email"       => $user->getEmail(),
			"phone"       => $user->getPhone(),
			"address"     => $user->getAddress(),
		) );
	}

	/*public function totalProperties()
	{
		$users = array();
		$query    = $this->db->query( "SELECT * FROM tbl_users WHERE id IN (select * from tbl_property where id IN (select * from tbl_property_room where room_id IN (select * from tbl_property_photos where details_id = 1 )))" ); //whenever you want to select everything from your table
		$result   = $query->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $result as $r ) {
			$users[] = new UserBean( $r );
		}

		return $users;
	}*/
}