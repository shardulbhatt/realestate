<?php

class VisitDBManager extends DBManager {
	protected $db;

	public function __construct() {
		$this->db = DBManager::Instance()->getDb();
	}

	public function getAllVisits() {
		$visits = array();
		$query    = $this->db->query( "SELECT * FROM tbl_visits ORDER BY id DESC" ); //whenever you want to select everything from your table
		$result   = $query->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $result as $r ) {
			$visits[] = new VisitBean( $r );
		}

		return $visits;
	}

	public function getSingleVisit( $id ) {
		$query = $this->db->prepare( "SELECT * FROM tbl_visits WHERE id = ?" ); //whenever you want to select a single record from your table
		$query->execute( array( $id ) );
		$result = $query->fetch( PDO::FETCH_ASSOC );

		return new VisitBean( $result );
	}

	public function addVisit( $visit ) {
		$query = $this->db->prepare( "INSERT INTO tbl_visits VALUES (DEFAULT, :tbl_users_id, :tbl_property_id, :user_coments  ,DEFAULT)" );
		$query->execute( array(
			"tbl_users_id"    => $visit->getTbl_users_id(),
			"tbl_property_id"    => $visit->getTbl_property_id(),
			"user_coments"      => $visit->getUser_coments()
			
		) );
	}

	public function editVisit( $visit ) {
		$query = $this->db->prepare( "UPDATE tbl_visits SET tbl_users_id = :tbl_users_id, tbl_property_id = :tbl_property_id, user_coments = :user_coments,status = :status WHERE id = :id" );
		$query->execute( array(
			"id"       => $visit->getId(),
			"tbl_users_id"    => $visit->getTbl_users_id(),
			"tbl_property_id"    => $visit->getTbl_property_id(),
			"user_coments"      => $visit->getUser_coments(),
			"status"   => $visit->getStatus
			()
		) );
	}

	public function deleteVisit($id){
		$query = $this->db->prepare("DELETE FROM tbl_visits WHERE id = :id");
		$query->execute(array("id" => $id));
	}

	public function banVisit($id){
		$query = $this->db->prepare("UPDATE tbl_visits SET status = 0 WHERE id = :id");
		$query->execute(array("id" => $id));
	}

}

?>