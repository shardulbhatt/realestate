<?php 
class PropertyRoomBean
{
	private $room_id;
	private $listing_id;
	private $type;
	private $width;
	private $length;
	private $level;
	private $dimension;
	private $custom_room;
	private $tbl_property_id;
	private $status;

	public function __construct($data){

		$this->room_id	=		$data['room_id']??null;
		$this->listing_id=		$data['listing_id'];
		$this->type=			$data['type'];
		$this->width=			$data['width'];
		$this->length=			$data['length'];
		$this->level=			$data['level'];
		$this->dimension=		$data['dimension'];
		$this->custom_room=		$data['custom_room'];
		$this->tbl_property_id=	$data['tbl_property_id'];
		$this->status 		=	$data['status'];
	}

	public function getRoom_id(){
		return $this->room_id;
	}

	public function setRoom_id($room_id){
		$this->room_id = $room_id;
	}

	public function getListing_id(){
		return $this->listing_id;
	}

	public function setListing_id($listing_id){
		$this->listing_id = $listing_id;
	}

	public function getType(){
		return $this->type;
	}

	public function setType($type){
		$this->type = $type;
	}

	public function getWidth(){
		return $this->width;
	}

	public function setWidth($width){
		$this->width = $width;
	}

	public function getLength(){
		return $this->length;
	}

	public function setLength($length){
		$this->length = $length;
	}

	public function getLevel(){
		return $this->level;
	}

	public function setLevel($level){
		$this->level = $level;
	}

	public function getDimension(){
		return $this->dimension;
	}

	public function setDimension($dimension){
		$this->dimension = $dimension;
	}

	public function getCustom_room(){
		return $this->custom_room;
	}

	public function setCustom_room($custom_room){
		$this->custom_room = $custom_room;
	}

	public function getTbl_property_id(){
		return $this->tbl_property_id;
	}

	public function setTbl_property_id($tbl_property_id){
		$this->tbl_property_id = $tbl_property_id;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}

}

?>