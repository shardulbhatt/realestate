<?php 

class PropertyBean 
{
	private $property_id;
	private $tbl_users_id;
	private $last_updated;
	private $features;
	private $parking_space_total;
	private $price;
	private $property_type;
	private $public_remarks;
	private $structure;
	private $bathroom_total;
	private $bedroom_total;
	private $architectural_style;
	private $constructed_date;
	private $exterior_finish;
	private $flooring_type;
	private $halfbath_total;
	private $renovated_date;
	private $stories_total;
	private $size_exterior;
	private $size_interior;
	private $street_address;
	private $city;
	private $province;
	private $postalcode;
	private $country;
	private $community_name;
	private $neighbourhood;
	private $subdivision;
	private $status;

	public function __construct($data){

		$this->property_id				= $data['property_id']?? null;
		$this->tbl_users_id				= $data['tbl_users_id'];
		$this->last_updated				= $data['last_updated'];
		$this->features					= $data['features'];
		$this->parking_space_total		= $data['parking_space_total'];
		$this->price 					= $data['price'];
		$this->property_type			= $data['property_type'];
		$this->public_remarks			= $data['public_remarks'];
		$this->structure 				= $data['structure'];
		$this->bathroom_total			= $data['bathroom_total'];
		$this->bedroom_total			= $data['bedroom_total'];
		$this->architectural_style		= $data['architectural_style'];
		$this->constructed_date			= $data['constructed_date'];
		$this->exterior_finish			= $data['exterior_finish'];
		$this->flooring_type			= $data['flooring_type'];
		$this->halfbath_total			= $data['halfbath_total'];
		$this->renovated_date			= $data['renovated_date'];
		$this->stories_total			= $data['stories_total'];
		$this->size_exterior			= $data['size_exterior'];
		$this->size_interior			= $data['size_interior'];
		$this->street_address			= $data['street_address'];
		$this->city 					= $data['city'];
		$this->province					= $data['province'];
		$this->postalcode				= $data['postalcode'];
		$this->country 					= $data['country'];
		$this->community_name			= $data['community_name'];
		$this->neighbourhood			= $data['neighbourhood'];
		$this->subdivision				= $data['subdivision'];
		$this->status 					= $data['status']??1;
	}

			public function getProperty_id(){
		return $this->property_id;
	}

	public function setProperty_id($property_id){
		$this->property_id = $property_id;
	}

	public function getTbl_users_id(){
		return $this->tbl_users_id;
	}

	public function setTbl_users_id($tbl_users_id){
		$this->tbl_users_id = $tbl_users_id;
	}

	public function getLast_updated(){
		return $this->last_updated;
	}

	public function setLast_updated($last_updated){
		$this->last_updated = $last_updated;
	}

	public function getFeatures(){
		return $this->features;
	}

	public function setFeatures($features){
		$this->features = $features;
	}

	public function getParking_space_total(){
		return $this->parking_space_total;
	}

	public function setParking_space_total($parking_space_total){
		$this->parking_space_total = $parking_space_total;
	}

	public function getPrice(){
		return $this->price;
	}

	public function setPrice($price){
		$this->price = $price;
	}

	public function getProperty_type(){
		return $this->property_type;
	}

	public function setProperty_type($property_type){
		$this->property_type = $property_type;
	}

	public function getPublic_remarks(){
		return $this->public_remarks;
	}

	public function setPublic_remarks($public_remarks){
		$this->public_remarks = $public_remarks;
	}

	public function getStructure(){
		return $this->structure;
	}

	public function setStructure($structure){
		$this->structure = $structure;
	}

	public function getBathroom_total(){
		return $this->bathroom_total;
	}

	public function setBathroom_total($bathroom_total){
		$this->bathroom_total = $bathroom_total;
	}

	public function getBedroom_total(){
		return $this->bedroom_total;
	}

	public function setBedroom_total($bedroom_total){
		$this->bedroom_total = $bedroom_total;
	}

	public function getArchitectural_style(){
		return $this->architectural_style;
	}

	public function setArchitectural_style($architectural_style){
		$this->architectural_style = $architectural_style;
	}

	public function getConstructed_date(){
		return $this->constructed_date;
	}

	public function setConstructed_date($constructed_date){
		$this->constructed_date = $constructed_date;
	}

	public function getExterior_finish(){
		return $this->exterior_finish;
	}

	public function setExterior_finish($exterior_finish){
		$this->exterior_finish = $exterior_finish;
	}

	public function getFlooring_type(){
		return $this->flooring_type;
	}

	public function setFlooring_type($flooring_type){
		$this->flooring_type = $flooring_type;
	}

	public function getHalfbath_total(){
		return $this->halfbath_total;
	}

	public function setHalfbath_total($halfbath_total){
		$this->halfbath_total = $halfbath_total;
	}

	public function getRenovated_date(){
		return $this->renovated_date;
	}

	public function setRenovated_date($renovated_date){
		$this->renovated_date = $renovated_date;
	}

	public function getStories_total(){
		return $this->stories_total;
	}

	public function setStories_total($stories_total){
		$this->stories_total = $stories_total;
	}

	public function getSize_exterior(){
		return $this->size_exterior;
	}

	public function setSize_exterior($size_exterior){
		$this->size_exterior = $size_exterior;
	}

	public function getSize_interior(){
		return $this->size_interior;
	}

	public function setSize_interior($size_interior){
		$this->size_interior = $size_interior;
	}

	public function getStreet_address(){
		return $this->street_address;
	}

	public function setStreet_address($street_address){
		$this->street_address = $street_address;
	}

	public function getCity(){
		return $this->city;
	}

	public function setCity($city){
		$this->city = $city;
	}

	public function getProvince(){
		return $this->province;
	}

	public function setProvince($province){
		$this->province = $province;
	}

	public function getPostalcode(){
		return $this->postalcode;
	}

	public function setPostalcode($postalcode){
		$this->postalcode = $postalcode;
	}

	public function getCountry(){
		return $this->country;
	}

	public function setCountry($country){
		$this->country = $country;
	}

	public function getCommunity_name(){
		return $this->community_name;
	}

	public function setCommunity_name($community_name){
		$this->community_name = $community_name;
	}

	public function getNeighbourhood(){
		return $this->neighbourhood;
	}

	public function setNeighbourhood($neighbourhood){
		$this->neighbourhood = $neighbourhood;
	}

	public function getSubdivision(){
		return $this->subdivision;
	}

	public function setSubdivision($subdivision){
		$this->subdivision = $subdivision;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}


}

?>