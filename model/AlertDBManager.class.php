<?php 

class AlertDBManager extends DBManager {
	protected $db;

	/**
	 * Student constructor.
	 */
	public function __construct() {
		$this->db = DBManager::Instance()->getDb();
	}

	public function getAllAlerts() {
		$alerts = array();
		$query    = $this->db->query( "SELECT * FROM tbl_alerts ORDER BY id DESC" ); //whenever you want to select everything from your table
		$result   = $query->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $result as $r ) {
			$alerts[] = new AlertBean( $r );
		}

		return $alerts;
	}


	public function getSingleAlert( $id ) {
		$query = $this->db->prepare( "SELECT * FROM tbl_alerts WHERE id = ?" ); //whenever you want to select a single record from your table
		$query->execute( array( $id ) );
		$result = $query->fetch( PDO::FETCH_ASSOC );

		return new AlertBean( $result );
	}

	public function addAlert( $alert ) {
		$query = $this->db->prepare( "INSERT INTO tbl_alerts VALUES (DEFAULT, :search_criteria, DEFAULT,:tbl_users_id)" );

		$query->execute( array(
			"search_criteria"    => $alert->getSearch_criteria(),
			"tbl_users_id"    => $alert->getTbl_users_id(),
			
		) );
	}

	public function editAlert( $alert ) {
		$query = $this->db->prepare( "UPDATE tbl_alerts SET search_criteria = :search_criteria, status = :status,tbl_users_id = :tbl_users_id  WHERE id = :id" );
		$query->execute( array(
			"id"       => $alert->getId(),
			"search_criteria" =>$alert->getSearch_criteria(),
			"status"   => $alert->getStatus(),
			"tbl_users_id"=>$alert->getTbl_users_id()
		) );
	}

	public function deleteAlert($id){
		$query = $this->db->prepare("DELETE FROM tbl_alerts WHERE id = :id");
		$query->execute(array("id" => $id));
	}

	public function banAlert($id){
		$query = $this->db->prepare("UPDATE tbl_alerts SET status = 0 WHERE id = :id");
		$query->execute(array("id" => $id));
	}

}

?>