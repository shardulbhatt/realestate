<?php
include "head.php";
include "Container.php";
include "header.php";
//$coreController = "Controller/UserController.php";
//$user_db = new UserDBManager();

$coreController = "./controller/PropertyController.php";
$property_db = new PropertyDBManager();


?>
<?php //unset($_SESSION['msg']); ?>

    <div class="super_container">
        <!-- Home -->

        <div class="home">
            <div class="parallax_background parallax-window" data-parallax="scroll"
                 style="background-image:url(assets/images/properties.jpg)" data-speed="0.8"></div>
            <div class="home_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="home_content d-flex flex-row align-items-end justify-content-start">
                                <div class="home_title">Search results</div>
                                <div class="breadcrumbs ml-auto">
                                    <ul>
                                        <li><a href="index.htmo">Home</a></li>
                                        <li>Search results</li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Home Search -->
        <div class="home_search">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_search_container">
                            <div class="home_search_content">
                                <form action="#"
                                      class="search_form d-flex flex-row align-items-start justfy-content-start">
                                    <div class="search_form_content d-flex flex-row align-items-start justfy-content-start flex-wrap">
                                        <div>
                                            <select class="search_form_select">
                                                <option disabled selected>For rent</option>
                                                <option>Yes</option>
                                                <option>No</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="search_form_select">
                                                <option disabled selected>All types</option>
                                                <option>Type 1</option>
                                                <option>Type 2</option>
                                                <option>Type 3</option>
                                                <option>Type 4</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="search_form_select">
                                                <option disabled selected>City</option>
                                                <option>New York</option>
                                                <option>Paris</option>
                                                <option>Amsterdam</option>
                                                <option>Rome</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="search_form_select">
                                                <option disabled selected>Bedrooms</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                            </select>
                                        </div>
                                        <div>
                                            <select class="search_form_select">
                                                <option disabled selected>Bathrooms</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button class="search_form_button ml-auto">search</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="properties">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="section_title">25 Properties found</div>
                        <div class="section_subtitle">Search your dream home</div>
                    </div>
                    <div class="pull-right">
                    <?php if (isset($_SESSION['loginid'])) : ?>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#postproperty">Post Property
                        </button>
                    <?php endif; ?>
                    </div>
                </div>
                <div id="postproperty" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Post Property</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Last Updated</label>
    
                                            <input type="date" id="text-input" name="last_updated" placeholder="Last Updated" class="form-control" required="required">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
                                <div class="form-group">
                                    <div class="clearfix">
                                        <label>Features</label>
                                        <!-- <a href="#" class="pull-right text-muted"><small>Forgot?</small></a> -->
                                    </div>

                                    <textarea name="features" id="textarea-input" rows="5" placeholder="Features..." class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                <label>Parking Space</label>
    
                                           <input type="text" id="text-input" name="parking_space_total" placeholder="Parking Space" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
                                <div class="form-group">
                                <label>Price</label>
    
                                            <input type="number" id="text-input" name="price" placeholder="Price" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
                                <div class="form-group">
                                <label>Property Type</label>
    
                                            <input type="text" id="text-input" name="property_type" placeholder="Property Type" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
                                <div class="form-group">
                                <label>Public Remarks</label>
    
                                            <textarea name="public_remarks" id="textarea-input" rows="5" placeholder="Public Remarks..." class="form-control"></textarea>
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
                                <div class="form-group">
                                <label>Structure</label>
    
                                            <input type="text" id="text-input" name="structure" placeholder="Structure" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
                                <div class="form-group">
                                <label>Total Bathrooms</label>
    
                                            <input type="number" id="text-input" name="bathroom_total" placeholder="Number Of Bathrooms" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
                                <div class="form-group">
                                <label>Total Bedrooms</label>
    
                                            <input type="number" id="text-input" name="bedroom_total" placeholder="Number Of Bedrooms" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Architectural Style</label>
    
                                            <input type="text" id="email-input" name="architectural_style" placeholder="Architectural Style" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Construction Date</label>
    
                                            <input type="date" id="text-input" name="constructed_date" placeholder="Construction Date" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Exterior Finish</label>
    
                                            <input type="text" id="text-input" name="exterior_finish" placeholder="Exterior Finish" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Flooring Type</label>
    
                                            <input type="text" id="text-input" name="flooring_type" placeholder="Flooring Type" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Total HalfBathrooms</label>
    
                                            <input type="number" id="text-input" name="halfbath_total" placeholder="Number Of Half Bathrooms" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Renovated Date</label>
    
                                            <input type="date" id="text-input" name="renovated_date" placeholder="Renovated Date" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Total Storeis</label>
    
                                            <input type="number" id="text-input" name="stories_total" placeholder="Total Storeis" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Size Exterior</label>
    
                                            <input type="text" id="text-input" name="   size_exterior" placeholder="Size Exterior" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Size Interior</label>
    
                                            <input type="text" id="text-input" name="   size_interior" placeholder="Size Interior" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Street Address</label>
    
                                            <textarea name="street_address" id="textarea-input" rows="5" placeholder="Street Address..." class="form-control"></textarea>
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>City</label>
    
                                            <input type="text" id="text-input" name="city" placeholder="City" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Province</label>
    
                                            <select name="province" id="select" class="form-control">
                                                <option value="0">Please select Province</option>
                                                <option value="Ontario">Ontario</option>
                                                <option value="Quebec">Quebec</option>
                                                <option value="Alberta">Alberta</option>
                                                <option value="British Columbia">British Columbia</option>
                                            </select>
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Postal Code</label>
    
                                            <input type="text" id="text-input" name="postalcode" placeholder="Postal Code" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Country</label>
    
                                            <input type="text" name="country" class="form-control" value="Canada" readonly>
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Community Name</label>
    
                                            <input type="text" id="text-input" name="community_name" placeholder="Community Name" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Neighbourhood</label>
    
                                            <input type="text" id="text-input" name="neighbourhood" placeholder="Neighbourhood" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
<div class="form-group">
                                <label>Sub Division</label>
    
                                             <input type="text" id="text-input" name="subdivision" placeholder="Sub Division" class="form-control">
                                            <!-- <small class="form-text text-muted">This is a help text</small> -->
                                </div>
                                    


                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-default" data-dismiss="modal" name="postproperty">Post</button>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="row properties_row">

                    <!-- Property -->
                    <?php foreach ($property_db->getAllProperties() as $properties) : ?>
                    <div class="col-xl-4 col-lg-6 property_col">
                        <div class="property">
                            <div class="property_image">
                                <img src="assets/images/property_1.jpg" alt="">
                                <div class="tag_featured property_tag"><a href="#">Featured</a></div>
                            </div>
                            <div class="property_body text-center">
                                <div class="property_location"><?= $properties->getCity() ?></div>
                                <div class="property_title"><a href="property.html"><?= $properties->getFeatures() ?></a></div>
                                <div class="property_price">$ <?= $properties->getPrice() ?></div>
                            </div>
                            <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                    <span><?= $properties->getSize_exterior() ?> Ftsq </span></div>
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                    <span> <?= $properties->getBedroom_total() ?> Bedrooms</span></div>
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                    <span><?= $properties->getBathroom_total() ?> Bathrooms</span></div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <!-- Property -->
                    <div class="col-xl-4 col-lg-6 property_col">
                        <div class="property">
                            <div class="property_image">
                                <img src="assets/images/property_2.jpg" alt="">
                                <div class="tag_offer property_tag"><a href="#">Offer</a></div>
                            </div>
                            <div class="property_body text-center">
                                <div class="property_location">Los Angeles</div>
                                <div class="property_title"><a href="property.html">2 Floor Town House</a></div>
                                <div class="property_price">$ 1. 234 981</div>
                            </div>
                            <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                    <span>650 Ftsq</span></div>
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                    <span>3 Bedrooms</span></div>
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                    <span>3 Bathrooms</span></div>
                            </div>
                        </div>
                    </div>

                    <!-- Property -->
                    <div class="col-xl-4 col-lg-6 property_col">
                        <div class="property">
                            <div class="property_image">
                                <img src="assets/images/property_3.jpg" alt="">
                                <div class="tag_featured property_tag"><a href="#">Featured</a></div>
                            </div>
                            <div class="property_body text-center">
                                <div class="property_location">Florida</div>
                                <div class="property_title"><a href="property.html">Vacation Home</a></div>
                                <div class="property_price">$ 1. 234 981</div>
                            </div>
                            <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                    <span>650 Ftsq</span></div>
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                    <span>3 Bedrooms</span></div>
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                    <span>3 Bathrooms</span></div>
                            </div>
                        </div>
                    </div>

                    <!-- Property -->
                    <div class="col-xl-4 col-lg-6 property_col">
                        <div class="property">
                            <div class="property_image">
                                <img src="assets/images/property_4.jpg" alt="">
                                <div class="tag_new property_tag"><a href="#">New</a></div>
                            </div>
                            <div class="property_body text-center">
                                <div class="property_location">Miami</div>
                                <div class="property_title"><a href="property.html">Sea view property</a></div>
                                <div class="property_price">$ 1. 234 981</div>
                            </div>
                            <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                    <span>650 Ftsq</span></div>
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                    <span>3 Bedrooms</span></div>
                                <div>
                                    <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                    <span>3 Bathrooms</span></div>
                            </div>
                        </div>
                    </div>

                    <!-- Property -->
                  <!--   <div class="col-xl-4 col-lg-6 property_col">
                      <div class="property">
                          <div class="property_image">
                              <img src="assets/images/property_5.jpg" alt="">
                          </div>
                          <div class="property_body text-center">
                              <div class="property_location">New York</div>
                              <div class="property_title"><a href="property.html">Sea view property</a></div>
                              <div class="property_price">$ 1. 234 981</div>
                          </div>
                          <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                  <span>650 Ftsq</span></div>
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                  <span>3 Bedrooms</span></div>
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                  <span>3 Bathrooms</span></div>
                          </div>
                      </div>
                  </div>
                  
                  Property
                  <div class="col-xl-4 col-lg-6 property_col">
                      <div class="property">
                          <div class="property_image">
                              <img src="assets/images/property_6.jpg" alt="">
                          </div>
                          <div class="property_body text-center">
                              <div class="property_location">Miami</div>
                              <div class="property_title"><a href="property.html">Sea view property</a></div>
                              <div class="property_price">$ 1. 234 981</div>
                          </div>
                          <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                  <span>650 Ftsq</span></div>
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                  <span>3 Bedrooms</span></div>
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                  <span>3 Bathrooms</span></div>
                          </div>
                      </div>
                  </div>
                  
                  Property
                  <div class="col-xl-4 col-lg-6 property_col">
                      <div class="property">
                          <div class="property_image">
                              <img src="assets/images/property_7.jpg" alt="">
                              <div class="tag_new property_tag"><a href="#">New</a></div>
                          </div>
                          <div class="property_body text-center">
                              <div class="property_location">Miami</div>
                              <div class="property_title"><a href="property.html">Sea view property</a></div>
                              <div class="property_price">$ 1. 234 981</div>
                          </div>
                          <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                  <span>650 Ftsq</span></div>
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                  <span>3 Bedrooms</span></div>
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                  <span>3 Bathrooms</span></div>
                          </div>
                      </div>
                  </div>
                  
                  Property
                  <div class="col-xl-4 col-lg-6 property_col">
                      <div class="property">
                          <div class="property_image">
                              <img src="assets/images/property_8.jpg" alt="">
                          </div>
                          <div class="property_body text-center">
                              <div class="property_location">San Francisco</div>
                              <div class="property_title"><a href="property.html">Sea view property</a></div>
                              <div class="property_price">$ 1. 234 981</div>
                          </div>
                          <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                  <span>650 Ftsq</span></div>
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                  <span>3 Bedrooms</span></div>
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                  <span>3 Bathrooms</span></div>
                          </div>
                      </div>
                  </div>
                  
                  Property
                  <div class="col-xl-4 col-lg-6 property_col">
                      <div class="property">
                          <div class="property_image">
                              <img src="assets/images/property_9.jpg" alt="">
                          </div>
                          <div class="property_body text-center">
                              <div class="property_location">Miami</div>
                              <div class="property_title"><a href="property.html">Sea view property</a></div>
                              <div class="property_price">$ 1. 234 981</div>
                          </div>
                          <div class="property_footer d-flex flex-row align-items-center justify-content-start">
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_1.png" alt=""></div>
                                  <span>650 Ftsq</span></div>
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_2.png" alt=""></div>
                                  <span>3 Bedrooms</span></div>
                              <div>
                                  <div class="property_icon"><img src="assets/images/icon_3.png" alt=""></div>
                                  <span>3 Bathrooms</span></div>
                          </div>
                      </div>
                  </div>
                  
                                  </div>
                                  <div class="row">
                  <div class="col">
                      <div class="pagination">
                          <ul>
                              <li class="active"><a href="#">01.</a></li>
                              <li><a href="#">02.</a></li>
                              <li><a href="#">03.</a></li>
                              <li><a href="#">04.</a></li>
                          </ul>
                      </div>
                  </div>
                                  </div>
                              </div>
                          </div>
                   -->

        <!-- Newsletter -->

        <div class="newsletter">
            <div class="parallax_background parallax-window" style="background-image:url(assets/images/newsletter.jpg)"
                 data-speed="0.8"></div>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="newsletter_content d-flex flex-lg-row flex-column align-items-start justify-content-start">
                            <div class="newsletter_title_container">
                                <div class="newsletter_title">Are you buying or selling?</div>
                                <div class="newsletter_subtitle">Search your dream home</div>
                            </div>
                            <div class="newsletter_form_container">
                                <form action="#" class="newsletter_form">
                                    <input type="email" class="newsletter_input" placeholder="Your e-mail address"
                                           required="required">
                                    <button class="newsletter_button">subscribe now</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/styles/bootstrap4/popper.js"></script>
    <script src="assets/styles/bootstrap4/bootstrap.min.js"></script>
    <script src="assets/plugins/greensock/TweenMax.min.js"></script>
    <script src="assets/plugins/greensock/TimelineMax.min.js"></script>
    <script src="assets/plugins/scrollmagic/ScrollMagic.min.js"></script>
    <script src="assets/plugins/greensock/animation.gsap.min.js"></script>
    <script src="assets/plugins/greensock/ScrollToPlugin.min.js"></script>
    <script src="assets/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
    <script src="assets/plugins/easing/easing.js"></script>
    <script src="assets/plugins/parallax-js-master/parallax.min.js"></script>
    <script src="assets/js/properties.js"></script>
<?php
include "footer.php";
if (isset($_POST['postproperty'])) {
    $pc = new PropertyController();
    $pc->addProperty($_POST);
}
if(isset($_POST['addUser']))
    {
        $uc = new UserController();
        $uc->addUser($_POST);
    }
?>