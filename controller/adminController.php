<?php
session_start();
include "../model/UserBean.class.php";
include "../model/DBManager.class.php";
include "../model/UserDBManager.class.php";
include "../model/PropertyBean.class.php";
include "../model/PropertyDBManager.class.php";
include "../model/PropertyRoomBean.class.php";
include "../model/PropertyRoomDBManager.class.php";
include "../model/PropertyPhotosDBManager.class.php";
include "../model/PropertyPhotosBean.class.php";


class adminController {

    private $loginid = null;
    
    public function __construct() 
    {
        if (!isset($_SESSION['loginid'])) 
        {
			echo "<script> window.location = '../login.php';</script>";
        }
        else
        {
            $udb = new UserDBManager();
            
            $udata = $udb->getSingleUserByUsername($_SESSION['loginid']);

            $this->loginid = $udata->getId();
            
            if($udata && ($udata->getLevel() == 1 || $udata->getLevel() == 2))
            {
                echo "<script> window.location = '../updateProfile.php';</script>";
            }
        }
    }
   
}
?>