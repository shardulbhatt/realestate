<?php

class RoomController 
{
	public function addRoom($roomdata, $pid)
	{
        $rdb = new PropertyRoomDBManager();
        
        try
        {
            $roomdata['tbl_property_id'] = $pid;
            $roomid = $rdb->addRoom( new PropertyRoomBean($roomdata) );
            echo "<script> window.location = './admin-properties.php?propertydata=success&property=$pid&room=$roomid';</script>";
        }
        catch(Exception $e)
        {
            echo "<script> window.location = './admin-addNewProperty.php?error=1004';</script>";
        }

	}



    public function getSingleRoom($id)
        {
             $rdb = new PropertyRoomDBManager();
            $roomdata = $rdb->getSingleRoom($id);
            return $roomdata;
        }

    public function updateRoom($roomdata)
    {
        $rdb = new PropertyRoomDBManager();
        try
                {
                    $rdb->editRoom(new PropertyRoomBean($roomdata));

                    echo "<script> window.location = './admin-data_room.php';</script>";
                }
                catch(Exception $e)
                {
                    echo "<script> window.location = './admin-updateRoom.php?id=".$_POST[' tbl_property_id']."&error=1001';</script>";
                }
    }


    public function postRoom($roomdata, $pid)
    {
        $rdb = new PropertyRoomDBManager();
        
        try
        {
            $roomdata['tbl_property_id'] = $pid;
            $roomid = $rdb->addRoom( new PropertyRoomBean($roomdata) );
            echo "<script> window.location = './client_addNewPhoto.php?propertydata=success&property=$pid&room=$roomid';</script>";
        }
        catch(Exception $e)
        {
            echo "<script> window.location = './admin-addNewRoom.php?error=1004';</script>";
        }

    }


}
?>