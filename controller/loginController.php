<?php 

class loginController 
{
	private $db;

	public function __construct() {
		$this->db = new UserDBManager();
	}

	public function doLogin($credentials)
	{
		if(isset($credentials['getSingleUser']) && $credentials['password'] && $credentials['username']) 
		{
			$user = $this->db->checkLoginCredentials($credentials['username'],$credentials['password']);
		
			if($user)
			{
				$_SESSION['loginid'] = $credentials['username'];
		
				if($user['level'] == 1 || $user['level'] == 2 )
				{
					echo "<script> window.location = './updateProfile.php';</script>";
				}
				else
				{
					echo "<script> window.location = './view_admin/admin-dashboard.php';</script>";
				}		
			}
			else
			{
				echo "<script> window.location = './login.php?error=2000';</script>";
			}
		} 
		else 
		{
			echo "<script> window.location = './login.php?error=2001';</script>";
		}
	}

	public function checkLogin()
	{
		if (!isset($_SESSION['loginid'])) {
			echo "<script> window.location = './login.php';</script>";
		}
	}

	public function checkUserSession()
	{
		if (isset($_SESSION['loginid'])) {

			$udata = $this->getCurrentUserData();
			
			if($udata && ($udata->getLevel() == 1 || $udata->getLevel() == 2))
			{
				echo "<script> window.location = './updateProfile.php';</script>";
			}

			if($udata && $udata->getLevel() > 2)
			{
				$this->redirectToAdmin();
			}
		}
	}

	public function redirectToAdmin()
	{
		echo "<script> window.location = './view_admin/admin-dashboard.php';</script>";
	}

	public function getCurrentUserData()
	{
        return $this->db->getSingleUserByUsername($_SESSION['loginid']);				
	}
}


?>