<?php
	class UserController 
	{
		private $db;
		private $baseUrl;
		private $flag = true;
		
		public function __construct() {
			$this->db = new UserDBManager();
			$this->baseUrl = $_SERVER['SERVER_NAME'];
		}

		public function addUser($udata, $fromAdmin = false)
		{
			if($fromAdmin === false)
			{
				if($udata['fname'] && $udata['lname'] && $udata['username'] && $udata['email'] && $udata['password'] && $udata['confpass']) 
				{
					
					$isUsernameExist = $this->db->checkUsernameExistOrNot($_POST['username']);

					if($isUsernameExist)
					{
						echo "<script> window.location = './register.php?error=1000';</script>";
					}
					elseif(filter_var($udata['email'], FILTER_VALIDATE_EMAIL) === false) {
						echo "<script> window.location = './register.php?error=1001';</script>";
					}
					elseif($udata['password'] != $udata['confpass'])
					{
						echo "<script> window.location = './register.php?error=1002';</script>";
					}
					elseif (!preg_match("/^[4-9]\d{9}$/", $udata['phone']) ) {

						if (strlen($udata['phone'])<10 || strlen($udata['phone'])>10) {
							echo "<script> window.location = './register.php?error=1006';</script>";
						}
						
						echo "<script> window.location = './register.php?error=1005';</script>";	
					}
					else
					{
						try
						{
							$this->db->addUser( new UserBean( $udata ) );
							echo "<script> window.location = './login.php?registration=success';</script>";
						}
						catch(Exception $e)
						{
							echo "<script> window.location = './register.php?error=1004';</script>";
						}
					}
				}
				else
				{
					echo "<script> window.location = './register.php?error=1003';</script>";
				}
			}
			else
			{
				if($udata['fname'] && $udata['lname'] && $udata['username'] && $udata['email'] && $udata['password'] && $udata['confpass']) 
				{
					
					$isUsernameExist = $this->db->checkUsernameExistOrNot($_POST['username']);

					if($isUsernameExist)
					{
						echo "<script> window.location = './admin-addNewUser.php?error=1000';</script>";
					}
					elseif(filter_var($udata['email'], FILTER_VALIDATE_EMAIL) === false) {
						echo "<script> window.location = './admin-addNewUser.php?error=1001';</script>";
					}
					elseif($udata['password'] != $udata['confpass'])
					{
						echo "<script> window.location = './admin-addNewUser.php?error=1002';</script>";
					}
					elseif (!preg_match("/^[4-9]\d{9}$/", $udata['phone']) ) {

						if (strlen($udata['phone'])<10 || strlen($udata['phone'])>10) {
							echo "<script> window.location = './admin-addNewUser.php?error=1006';</script>";
						}
						
						echo "<script> window.location = './admin-addNewUser.php?error=1005';</script>";	
					}
					else
					{
						try
						{
							$this->db->addUser( new UserBean( $udata ) );
							echo "<script> window.location = './admin-addNewUser.php?registration=success';</script>";
						}
						catch(Exception $e)
						{
							echo "<script> window.location = './admin-addNewUser.php?error=1004';</script>";
						}
					}
				}
				else
				{
					echo "<script> window.location = './admin-addNewUser.php?error=1003';</script>";
				}
			}
		}

		public function getCurrentUserData()
		{
			if(isset($_SESSION['loginid']))
			{
				return $this->db->getSingleUserByUsername($_SESSION['loginid']);				
			}
		}

		public function getSingleUserById()
		{
			if(isset($_SESSION['loginid']))
			{
				return $this->db->getSingleUserById($_SESSION['loginid']);				
			}
		}



		public function editUser($udata, $fromAdmin = false)
		{

			if($fromAdmin === false)
			{

			if($udata['fullname'] && $udata['username'] && $udata['email'] && $udata['dob'] && $udata['phone'] && $udata['address']) 
			{
				if(filter_var($udata['email'], FILTER_VALIDATE_EMAIL) === false) 
				{
					echo "<script> window.location = './updateProfile.php?error=1001';</script>";
				}
				else
				{
					try
					{
						$udata['username'] = $_SESSION['loginid'];
						$this->db->editUserByUsername(new UserBean( $udata ));
						echo "<script> window.location = './updateProfile.php?success=true';</script>";
					}
					catch(Exception $e)
					{
						echo "<script> window.location = './updateProfile.php?error=1004';</script>";
					}
				}
			}
			else
			{
				echo "<script> window.location = './updateProfile.php?error=1003';</script>";
			}


		}

		else
		{

			if($udata['fullname'] && $udata['username'] && $udata['email'] && $udata['dob'] && $udata['phone'] && $udata['address']) 
			{
				if(filter_var($udata['email'], FILTER_VALIDATE_EMAIL) === false) 
				{
					echo "<script> window.location = './admin-updateUser.php?error=1001';</script>";
				}
				else
				{
					try
					{
						$udata['username'] = $_SESSION['loginid'];
						$this->db->editUserByUsername(new UserBean( $udata ));
						echo "<script> window.location = './admin-updateUser.php?success=true';</script>";
					}
					catch(Exception $e)
					{
						echo "<script> window.location = './admin-updateUser.php?error=1004';</script>";
					}
				}
			}
			else
			{
				echo "<script> window.location = './admin-updateUser.php?error=1003';</script>";
			}
		}

		}








		public function checkUserFromUserSide($checkForAdmin = false)
		{
			$udata = $this->getCurrentUserData();
			
			if($udata && $udata->getLevel() == 4 && $checkForAdmin === true)
			{
				return true;
			}
			else if($udata && ($udata->getLevel() == 1 || $udata->getLevel() == 2) && $checkForAdmin === false) 
			{
				return true;
			}
			return false;
		}


		public function deleteUser()
		{
			if(isset($_POST['id'])){
			$this->db->deleteUser($_GET['id']);
			$_SESSION['msg'] = "User ID " . $_POST['id'] . " was deleted successfully!";
			echo "<script> window.location = './admin-data_user.php?success=true';</script>";

		}else
			$_SESSION['msg'] = "Sorry, no student specified";
		}

		public function banUser()
		{
				if(isset($_POST['id'])){
			$this->db->banUser($_POST['id']);
			$_SESSION['msg'] = "User ID " . $_POST['id'] . " was banned successfully!";
			echo "<script> window.location = './admin-data_user.php?success=true';</script>";
		}else
			$_SESSION['msg'] = "Sorry, no student specified";
		}

		public function getSingleUser($id)
		{
			$user = $this->db->getSingleUser($id);
			return $user;
		}

		public function updateUser($udata)
		{
			if($udata['fullname'] && $udata['username'] && $udata['email'] && $udata['dob'] && $udata['phone'] && $udata['address']) 
			{
				try
				{
					$this->db->editUser(new UserBean( $udata ));
					echo "<script> window.location = './admin-data_user.php';</script>";
				}
				catch(Exception $e)
				{
					echo "<script> window.location = './admin-updateUser.php?id=".$_POST['id']."&error=1001';</script>";
				}
			}
			else
			{
				echo "<script> window.location = './admin-updateUser.php?id=".$_POST['id']."&error=1000';</script>";
			}
		}
	}
?>