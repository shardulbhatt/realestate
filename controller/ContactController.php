<?php


class ContactController 
{
		private $db;
		private $baseUrl;
		private $flag = true;
		
		public function __construct() {
			$this->db = new ContactDBManager();
			$this->baseUrl = $_SERVER['SERVER_NAME'];
		}


		public function addContact($cdata)
		{

			try
				{
					$this->db->addContact( new ContactBean( $cdata ) );
					echo "<script> window.location = './login.php?registration=success';</script>";
				}
				catch(Exception $e)
				{
				echo "<script> window.location = './register.php?error=1004';</script>";
				}

		}

		
}

?>