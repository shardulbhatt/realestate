<?php

class PhotoController 
{ 
	public function addPhoto($photodata, $pid)
	{
        $targetPath = "../assets/rooms/";

        $pdm = new PropertyPhotosDBManager();

        try
        {	$photodata['tbl_property_id'] = $pid;
           

            if(!is_dir($targetPath)) {
                mkdir($targetPath);
            }

            foreach ($_FILES['photos']['name'] as $key => $value) {
                $filename = time().$_FILES['photos']['name'][$key];
                $targetFile = $targetPath.$filename;
                move_uploaded_file($_FILES["photos"]["tmp_name"][$key], $targetFile);
                $photodata['photos'] = $filename;
                $pdm->addPhoto(new PropertyPhotosBean($photodata));
            }

            echo "<script> window.location = './admin-properties.php?success=true';</script>";
        }
        catch(Exception $e)
        {
            echo "<script> window.location = './admin-addNewPhoto.php?error=4001&property=$pid&room=$rid';</script>";
        }

	}

    public function getSinglePhoto($id)
        {
             $photodb = new PropertyPhotosDBManager();
            $photodata = $photodb->getSinglePhoto($id);
            return $photodata;
        }

    public function updatePhoto($photodata)
    {
        $photodb = new PropertyPhotosDBManager();
        try
                {
                    $photodb->editPhoto(new PropertyPhotosBean($photodata));

               //     echo "<script> window.location = './admin-data_photo.php';</script>";
                }
                catch(Exception $e)
                {
                    echo "<script> window.location = './admin-updatePhoto.php?id=".$_POST['tbl_property_id']."&error=1001';</script>";
                }
    }


    public function postPhoto($photodata, $pid)
    {
        $targetPath = "assets/rooms/";

        $pdm = new PropertyPhotosDBManager();

        try
        {   $photodata['tbl_property_id'] = $pid;
           

            if(!is_dir($targetPath)) {
                mkdir($targetPath);
            }

            foreach ($_FILES['photos']['name'] as $key => $value) {
                $filename = time().$_FILES['photos']['name'][$key];
                $targetFile = $targetPath.$filename;
                move_uploaded_file($_FILES["photos"]["tmp_name"][$key], $targetFile);
                $photodata['photos'] = $filename;
                $pdm->addPhoto(new PropertyPhotosBean($photodata));
            }

          //  echo "<script> window.location = './properties.php?success=true';</script>";
        }
        catch(Exception $e)
        {
            echo "<script> window.location = './client_addNewPhoto.php?error=4001&property=$pid&room=$rid';</script>";
        }

    }






}



?>