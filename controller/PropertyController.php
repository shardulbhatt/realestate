<?php

class PropertyController 
{
    
    public function addProperty($propertydata)
    {
        $pdb = new PropertyDBManager();

        if ($propertydata) 
        {
            try
            {
                $propertydata['tbl_users_id'] = $this->getuserid();
                $generatedID = $pdb->addProperty( new PropertyBean($propertydata) );
                echo "<script> window.location = './admin-properties.php?propertydata=success&id=$generatedID'</script>";
            }
            catch(Exception $e)
            {
                echo "<script> window.location = './admin-addNewProperty.php?error=1004';</script>";
            }
        }
    }

    private function getuserid()
    {  
        $udb = new UserDBManager();
        $udata = $udb->getSingleUserByUsername($_SESSION['loginid']);
        return $udata->getId();
    }

    public function postProperty($propertydata)
    {
        $pdb = new PropertyDBManager();

        if ($propertydata) 
        {
            try
            {
                $propertydata['tbl_users_id'] = $this->getuserid();
                $generatedID = $pdb->addProperty( new PropertyBean($propertydata) );
                echo "<script> window.location = './client_addNewRoom.php?propertydata=success&id=$generatedID'</script>";
            }
            catch(Exception $e)
            {
                echo "<script> window.location = './client_addNewProperty.php?error=1004';</script>";
            }
        }
    }

    public function getSingleProperty($id)
        {
            $pdb = new PropertyDBManager();
            $propertydata = $pdb->getSingleProperty($id);
            return $propertydata;
        }

    public function updateProperty($propertydata)
    {   
        $pdb = new PropertyDBManager();
        try
                {
                    $pdb->editProperty(new PropertyBean($propertydata));
                    echo "<script> window.location = './admin-data_property.php';</script>";
                }
                catch(Exception $e)
                {
                    echo "<script> window.location = './admin-updateProperty.php?id=".$_POST['property_id']."&error=1001';</script>";
                }
    }
}

?>