<?php
	
	include "head.php";
	include "Container.php";
	include "header.php";
	include "./controller/loginController.php";
	$lc = new loginController();
	$lc->checkUserSession();
	$status = (isset($_GET['success'])) ? $_GET['success'] : null ;  
	$error = (isset($_GET['error'])) ? $_GET['error'] : null ; 
	$reg =  (isset($_GET['registration'])) ? $_GET['registration'] : null ; 
?>

<div class="super_container">

	<!-- Header 
	<div class="home">
		<div class="parallax_background parallax-window" data-parallax="scroll" style="background-image:url(assets/images/elements.jpg)" data-speed="0.8"></div>
		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content d-flex flex-row align-items-end justify-content-start">
							<div class="home_title">Login</div>
							<div class="breadcrumbs ml-auto">
								<ul>
									<li><a href="index.htmo">Home</a></li>
									<li>Login</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>-->
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">

				<form class="login100-form validate-form" action="" method="post">
					<span class="login100-form-title p-b-34">
						Account Login
					</span>
					
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input class="input100" type="text" name="username" placeholder="User name" required>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input class="input100" type="password" name="password" placeholder="Password" required>
						<span class="focus-input100"></span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" name="getSingleUser" type="submit">
							Sign in
						</button>
	
						<?php if($error && $error == 2000): ?>
							<div class="alert alert-danger mt-2" role="alert">
								Invalid username or password !!
							</div>
						<?php elseif($error && $error == 2001): ?>
							<div class="alert alert-danger mt-2" role="alert">
								Username and password are requiresd
							</div>
						<?php elseif($reg && $reg == "success"): ?>
							<div class="alert alert-info mt-2" role="alert">
								You are register successfully please login.
							</div>
						<?php endif; ?>
					</div>
	
					<div class="w-full text-center p-t-27 p-b-239">
						<span class="txt1">
							Forgot
						</span>

						<a href="#" class="txt2">
							User name / password?
						</a>
					</div>

					<div class="w-full text-center">
						<a href="register.php" class="txt3">
							Sign Up
						</a>
					</div>
				</form>

				<div class="login100-more" style="background-image: url('assets/login_css/images/bg-01.jpg');"></div>
			</div>
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>
	
	</div>
	
	
	<script src="assets/login_css/images/icons/favicon.ico"></script>
	<script src = "assets/login_css/vendor/bootstrap/css/bootstrap.min.css"></script>
	<script src = "assets/login_css/fonts/font-awesome-4.7.0/css/font-awesome.min.css"></script>
	<script src = "assets/login_css/fonts/Linearicons-Free-v1.0.0/icon-font.min.css"></script>
	<script src = "assets/login_css/vendor/animate/animate.css"></script>
	<script src = "assets/login_css/vendor/css-hamburgers/hamburgers.min.css"></script>
	<script src = "assets/login_css/vendor/animsition/css/animsition.min.css"></script>
	<script src = "assets/login_css/vendor/select2/select2.min.css"></script>
	<script src = "assets/login_css/vendor/daterangepicker/daterangepicker.css"></script>
	<script src = "assets/login_css/css/util.css"></script>
	<script src = "assets/login_css/css/main.css"></script>

<?php
	include "footer.php";

	if(isset($_POST['getSingleUser']))
    {
		$lc->doLogin($_POST);
	}
?>